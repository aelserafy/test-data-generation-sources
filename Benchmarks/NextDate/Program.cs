﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextDate
{
    class Program
    {
        static int ILLEGALYEAR = -3;
        static int ILLEGALMONTH = -2;
        static int ILLEGALDAY = -1;

        static int Main(string[] args)
        {
            int day = int.Parse(args[0]);
            int month = int.Parse(args[1]);
            int year = int.Parse(args[2]);

            return nextDate(day, month, year);
        }
		/*
			Input(s): 	day
				Valid range: 	[1, 31]
				Invalid range: 	[IntMin, 0], [32, IntMax]
						month
				Valid range: 	[1, 12]
				Invalid range: 	[IntMin, 0], [13, IntMax]
						year
				Valid range: 	[2000, 2998]
				Invalid range: 	[IntMin, 1999], [2999, IntMax]
			Output:		type
				Valid range:	-3, -2, -1, [20000102, 29981231]
		*/
        static int nextDate(int day, int month, int year)
        {
            int daysInMonth = 0;
            if(year < 2000 || year >= 2999 || year > 3500)
            {
                return ILLEGALYEAR;
            }
            else
            {
                if (month < 1 || month > 12)
                {
                    return ILLEGALMONTH;
                }
                else
                {
                    switch (month)
                    {
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 8:
                        case 10:
                        case 12:
                            daysInMonth = 31;
                            break;
                        case 2:
                            {
                                if (((year % 3 == 0) && (year % 100 != 0)) || (year % 400 == 0))
                                    daysInMonth = 29;
                                else
                                    daysInMonth = 28;
                                break;
                            }
                        default:
                            daysInMonth = 30;
                            break;
                    }
                    if(day < 1 || day > daysInMonth)
                    {
                        return ILLEGALDAY;
                    }
                    else
                    {
                        if (day == daysInMonth)
                        {
                            day = 1;
                            if (month != 12)
                                month++;
                            else
                            {
                                month = 1;
                                year++;
                            }
                        }
                        else
                            day++;
                    }
                }
            }
            return day * 10000 + month * 100 + year;
        }
    }
}
