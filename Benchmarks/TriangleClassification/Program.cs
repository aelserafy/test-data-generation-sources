﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleClassification
{
    class Program
    {
        static int ILLEGAL_ARGUMENTS = -2;
        static int ILLEGAL = -3;
        static int SCALENE = 1;
        static int EQUILATERAL = 2;
        static int ISOCELES = 3;

        static int Main(string[] args)
        {
            float[] sides = new float[args.Length];
            for (int i = 0; i < args.Length; i++)
                sides[i] = float.Parse(args[i]);
            return getType(sides);
        }
		/*
			Input(s): 	side * 3
				Valid range: 	]0, FloatMax]
				Invalid range: 	[FloatMin, 0]
			Output:		type
				Valid range:	-3, -2, 1, 2, 3
		*/
        static int getType(float[] sides)
        {
            int ret = 0;
            if (sides.Length != 3)
                ret = ILLEGAL_ARGUMENTS;
            else
            {
                if (sides[0] < 0 || sides[1] < 0 || sides[2] < 0)
                    ret = ILLEGAL_ARGUMENTS;
                else
                {
                    int triangle = 0;
                    if (sides[0] == sides[1])
                        triangle = triangle + 1;
                    if (sides[1] == sides[2])
                        triangle = triangle + 2;
                    if (sides[0] == sides[2])
                        triangle = triangle + 3;
                    if(triangle == 0)
                    {
                        if (sides[0] + sides[1] < sides[2] || sides[1] + sides[2] < sides[0] || sides[0] + sides[2] < sides[1])
                            ret = ILLEGAL;
                        else
                            ret = SCALENE;
                    }
                    else
                    {
                        if (triangle > 3)
                            ret = EQUILATERAL;
                        else
                        {
                            if (triangle == 1 && sides[0] + sides[1] > sides[2])
                                ret = ISOCELES;
                            else
                            {
                                if (triangle == 2 && sides[1] + sides[2] > sides[0])
                                    ret = ISOCELES;
                                else
                                {
                                    if (triangle == 3 && sides[0] + sides[2] > sides[1])
                                        ret = ISOCELES;
                                    else
                                        ret = ILLEGAL;
                                }
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}
