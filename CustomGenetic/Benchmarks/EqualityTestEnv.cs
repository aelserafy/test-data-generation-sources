﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;
using DecisionsModel;

namespace CustomGenetic.Benchmarks
{
    class EqualityTestEnv : Environment
    {
        public EqualityTestEnv(bool equality)
            : base(equality ? "Equality" : "Inequality")
        {
            NumberOfInputs = 1;
            Condition condition = new Condition(0, inputs => inputs[0], equality ? RelationalOperator.EqualEqual : RelationalOperator.MoreThan, inputs => 5);

            Decision decision = new Decision(condition);

            AppDecisions = new List<Decision>() { decision };

            AssignIDsToDecisions();
        }

        public override int RunProgram(double[] inputs)
        {
            ResetAllDecisionEvaluations();
            bool equalEqual = RelationalOperator.EqualEqual == ((Condition)(AppDecisions[0].FirstOperand)).RelationalOperator;
            AppDecisions[0].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
            if (equalEqual && AreEqual(inputs[0], 5) || !equalEqual && inputs[0] > 5)
                return 1;
            return 0;
        }
    }
}
