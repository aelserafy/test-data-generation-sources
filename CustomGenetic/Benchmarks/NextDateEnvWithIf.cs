﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;
using DecisionsModel;

namespace CustomGenetic.Benchmarks
{
    /// <summary>
    /// This contains a modified implementation of NextDate that maps January to UseDouble ? 1.5 : 1 and July to UseDouble ? 7.5 : 7
    /// This is done to evaluate the effectiveness of the developed algorithm in finding solutions for floating point decimals when present
    /// </summary>
    public class NextDateEnvWithIf : Environment
    {
        public bool UseDoubleVersion { get; set; }

        public NextDateEnvWithIf(bool useDoubleVersion)
            : base(useDoubleVersion ? "NextDateWithIfDouble" : "NextDateWithIfInt")
        {
            NumberOfInputs = 3;

            UseDoubleVersion = useDoubleVersion;

            Condition condition1 = new Condition(0, inputs => inputs[2], RelationalOperator.LessThan, inputs => 2000);
            Condition condition2 = new Condition(1, inputs => inputs[2], RelationalOperator.MoreThanOrEqual, inputs => 2999);
            Condition condition3 = new Condition(2, inputs => inputs[2], RelationalOperator.MoreThan, inputs => 3500);
            Decision decision0 = new Decision(new Decision(condition1, condition2, LogicalOperatorType.Or), condition3, LogicalOperatorType.Or);

            Condition condition4 = new Condition(0, inputs => inputs[1], RelationalOperator.LessThan, inputs => UseDoubleVersion ? 1.5 : 1);
            Condition condition5 = new Condition(1, inputs => inputs[1], RelationalOperator.MoreThan, inputs => 12);
            Decision decision1 = new Decision(condition4, condition5, LogicalOperatorType.Or);

            Condition condition6 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => UseDoubleVersion ? 1.5 : 1);
            Decision decision2 = new Decision(condition6);

            Condition condition7 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 3);
            Decision decision3 = new Decision(condition7);

            Condition condition8 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 5);
            Decision decision4 = new Decision(condition8);

            Condition condition9 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => UseDoubleVersion ? 7.5 : 7);
            Decision decision5 = new Decision(condition9);

            Condition condition10 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 8);
            Decision decision6 = new Decision(condition10);

            Condition condition11 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 10);
            Decision decision7 = new Decision(condition11);

            Condition condition12 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 12);
            Decision decision8 = new Decision(condition12);

            Condition condition13 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 2);
            Decision decision9 = new Decision(condition13);

            Condition condition14 = new Condition(0, inputs => inputs[2] % 3, RelationalOperator.EqualEqual, inputs => 0);
            Condition condition15 = new Condition(1, inputs => inputs[2] % 100, RelationalOperator.NotEqual, inputs => 0);
            Condition condition16 = new Condition(2, inputs => inputs[2] % 400, RelationalOperator.EqualEqual, inputs => 0);
            Decision decision10 = new Decision(new Decision(condition14, condition15, LogicalOperatorType.And), condition16, LogicalOperatorType.Or);

            Condition condition17 = new Condition(0, inputs => inputs[0], RelationalOperator.LessThan, inputs => 1);
            Condition condition18 = new Condition(1, inputs => inputs[0], RelationalOperator.MoreThan, GetDaysInMonth);
            Decision decision11 = new Decision(condition17, condition18, LogicalOperatorType.Or);

            Condition condition19 = new Condition(0, inputs => inputs[0], RelationalOperator.EqualEqual, GetDaysInMonth);
            Decision decision12 = new Decision(condition19);

            Condition condition20 = new Condition(0, inputs => inputs[1], RelationalOperator.NotEqual, inputs => 12);
            Decision decision13 = new Decision(condition20);

            decision1.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision0, false } });
            decision2.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
            decision3.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision2, false } });
            decision4.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision3, false } });
            decision5.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision4, false } });
            decision6.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false } });
            decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision6, false } });
            decision8.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision7, false } });
            decision9.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision8, false } });
            decision10.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision9, true } });
            decision11.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
            decision12.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision11, false } });
            decision13.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision12, true } });

            AppDecisions = new List<Decision>() { decision0, decision1, decision2, decision3, decision4, decision5, decision6, decision7, decision8, decision9, decision10, decision11, decision12, decision13 };

            AssignIDsToDecisions();
        }

        private double GetDaysInMonth(double[] inputs)
        {
            int daysInMonth;
            if (AreEqual(inputs[1], UseDoubleVersion ? 1.5 : 1) || AreEqual(inputs[1], 3) || AreEqual(inputs[1], 5) || AreEqual(inputs[1], UseDoubleVersion ? 7.5 : 7) || AreEqual(inputs[1], 8) || AreEqual(inputs[1], 10) || AreEqual(inputs[1], 12))
                daysInMonth = 31;
            else if (AreEqual(inputs[1], 2))
            {
                if (((AreEqual(inputs[2] % 3, 0)) && (!AreEqual(inputs[2] % 100, 0))) || (AreEqual(inputs[2] % 400, 0)))
                    daysInMonth = 29;
                else
                    daysInMonth = 28;
            }
            else
                daysInMonth = 30;

            return daysInMonth;
        }
        /*
            Input(s): 	day
                Valid range: 	[1, 31]
                Invalid range: 	[IntMin, 0], [32, IntMax]
                        month
                Valid range: 	[1, 12]
                Invalid range: 	[IntMin, 0], [13, IntMax]
                        year
                Valid range: 	[2000, 2998]
                Invalid range: 	[IntMin, 1999], [2999, IntMax]
            Output:		type
                Valid range:	-3, -2, -1, [20000102, 29981231]
        */

        public override int RunProgram(double[] inputs)
        {
            ResetAllDecisionEvaluations();

            int ILLEGALYEAR = -3;
            int ILLEGALMONTH = -2;
            int ILLEGALDAY = -1;
            double day = inputs[0], month = inputs[1], year = inputs[2];

            AppDecisions[0].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
            if (year < 2000 || year >= 2999 || year > 3500)
            {
                return ILLEGALYEAR;
            }
            else
            {
                AppDecisions[1].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                if (month < (UseDoubleVersion ? 1.5 : 1) || month > 12)
                {
                    return ILLEGALMONTH;
                }
                else
                {
                    AppDecisions[2].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    int daysInMonth;
                    if (AreEqual(month, UseDoubleVersion ? 1.5 : 1))
                    {
                        daysInMonth = 31;
                    }
                    else
                    {
                        AppDecisions[3].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                        if (AreEqual(month, 3))
                        {
                            daysInMonth = 31;
                        }
                        else
                        {
                            AppDecisions[4].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                            if (AreEqual(month, 5))
                            {
                                daysInMonth = 31;
                            }
                            else
                            {
                                AppDecisions[5].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                if (AreEqual(month, UseDoubleVersion ? 7.5 : 7))
                                {
                                    daysInMonth = 31;
                                }
                                else
                                {
                                    AppDecisions[6].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                    if (AreEqual(month, 8))
                                    {
                                        daysInMonth = 31;
                                    }
                                    else
                                    {
                                        AppDecisions[7].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                        if (AreEqual(month, 10))
                                        {
                                            daysInMonth = 31;
                                        }
                                        else
                                        {
                                            AppDecisions[8].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                            if (AreEqual(month, 12))
                                            {
                                                daysInMonth = 31;
                                            }
                                            else
                                            {
                                                AppDecisions[9].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                                if (AreEqual(month, 2))
                                                {
                                                    AppDecisions[10].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                                    if (((AreEqual(year % 3, 0)) && (!AreEqual(year % 100, 0))) || (AreEqual(year % 400, 0)))
                                                        daysInMonth = 29;
                                                    else
                                                        daysInMonth = 28;
                                                }
                                                else
                                                {
                                                    daysInMonth = 30;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    AppDecisions[11].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    if (day < 1 || day > daysInMonth)
                    {
                        return ILLEGALDAY;
                    }
                    else
                    {
                        AppDecisions[12].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                        if (AreEqual(day, daysInMonth))
                        {
                            day = 1;

                            AppDecisions[13].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                            if (!AreEqual(month, 12))
                                month++;
                            else
                            {
                                month = 1;
                                year++;
                            }
                        }
                        else
                            day++;
                    }
                }
            }
            return (int)day * 10000 + (int)month * 100 + (int)year;
        }
    }
}
