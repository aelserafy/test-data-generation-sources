﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;
using DecisionsModel;

namespace CustomGenetic.Benchmarks
{
    public class NextDateEnvWithSwitch : Environment
    {
        public NextDateEnvWithSwitch()
            : base("NextDateWithSwitch")
        {
            NumberOfInputs = 3;

            Condition condition1 = new Condition(0, inputs => inputs[2], RelationalOperator.LessThan, inputs => 2000);
            Condition condition2 = new Condition(1, inputs => inputs[2], RelationalOperator.MoreThanOrEqual, inputs => 2999);
            Condition condition3 = new Condition(2, inputs => inputs[2], RelationalOperator.MoreThan, inputs => 3500);
            Decision decision0 = new Decision(new Decision(condition1, condition2, LogicalOperatorType.Or), condition3, LogicalOperatorType.Or);

            Condition condition4 = new Condition(0, inputs => inputs[1], RelationalOperator.LessThan, inputs => 1);
            Condition condition5 = new Condition(1, inputs => inputs[1], RelationalOperator.MoreThan, inputs => 12);
            Decision decision1 = new Decision(condition4, condition5, LogicalOperatorType.Or);

            Condition condition6 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 1);
            Condition condition7 = new Condition(1, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 3);
            Condition condition8 = new Condition(2, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 5);
            Condition condition9 = new Condition(3, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 7);
            Condition condition10 = new Condition(4, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 8);
            Condition condition11 = new Condition(5, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 10);
            Condition condition12 = new Condition(6, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 12);
            Decision decision2 = new Decision(new Decision(new Decision(condition6, condition7, LogicalOperatorType.Or), new Decision(condition8, condition9, LogicalOperatorType.Or), LogicalOperatorType.Or), new Decision(new Decision(condition10, condition11, LogicalOperatorType.Or), condition12, LogicalOperatorType.Or), LogicalOperatorType.Or);

            Condition condition13 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => 2);
            Decision decision3 = new Decision(condition13);

            Condition condition14 = new Condition(0, inputs => inputs[2] % 3, RelationalOperator.EqualEqual, inputs => 0);
            Condition condition15 = new Condition(1, inputs => inputs[2] % 100, RelationalOperator.NotEqual, inputs => 0);
            Condition condition16 = new Condition(2, inputs => inputs[2] % 400, RelationalOperator.EqualEqual, inputs => 0);
            Decision decision4 = new Decision(new Decision(condition14, condition15, LogicalOperatorType.And), condition16, LogicalOperatorType.Or);

            Condition condition17 = new Condition(0, inputs => inputs[0], RelationalOperator.LessThan, inputs => 1);
            Condition condition18 = new Condition(1, inputs => inputs[0], RelationalOperator.MoreThan, inputs => GetDaysInMonth(inputs));
            Decision decision5 = new Decision(condition17, condition18, LogicalOperatorType.Or);

            Condition condition19 = new Condition(0, inputs => inputs[0], RelationalOperator.EqualEqual, inputs => GetDaysInMonth(inputs));
            Decision decision6 = new Decision(condition19);

            Condition condition20 = new Condition(0, inputs => inputs[1], RelationalOperator.NotEqual, inputs => 12);
            Decision decision7 = new Decision(condition20);

            decision1.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision0, false } });
            decision2.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
            decision3.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision2, false } });
            decision4.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision3, true } });
            decision5.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
            decision6.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false } });
            decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision6, true } });

            AppDecisions = new List<Decision>() { decision0, decision1, decision2, decision3, decision4, decision5, decision6, decision7 };

            AssignIDsToDecisions();
        }

        private int GetDaysInMonth(double[] inputs)
        {
            int daysInMonth;
            switch ((int)inputs[1])
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    daysInMonth = 31;
                    break;
                case 2:
                    {
                        if (((inputs[2] % 3 == 0) && (inputs[2] % 100 != 0)) || (inputs[2] % 400 == 0))
                            daysInMonth = 29;
                        else
                            daysInMonth = 28;
                        break;
                    }
                default:
                    daysInMonth = 30;
                    break;
            }
            return daysInMonth;
        }

        /*
			Input(s): 	day
				Valid range: 	[1, 31]
				Invalid range: 	[IntMin, 0], [32, IntMax]
						month
				Valid range: 	[1, 12]
				Invalid range: 	[IntMin, 0], [13, IntMax]
						year
				Valid range: 	[2000, 2998]
				Invalid range: 	[IntMin, 1999], [2999, IntMax]
			Output:		type
				Valid range:	-3, -2, -1, [20000102, 29981231]
		*/

        public override int RunProgram(double[] inputs)
        {
            ResetAllDecisionEvaluations();

            int ILLEGALYEAR = -3;
            int ILLEGALMONTH = -2;
            int ILLEGALDAY = -1;
            int day = (int)inputs[0], month = (int)inputs[1], year = (int)inputs[2]; //a switch does not accept doubles!

            AppDecisions[0].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
            if (year < 2000 || year >= 2999 || year > 3500)
            {
                return ILLEGALYEAR;
            }
            else
            {
                AppDecisions[1].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                if (month < 1 || month > 12)
                {
                    return ILLEGALMONTH;
                }
                else
                {
                    AppDecisions[2].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);

                    switch (month)
                    {
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 8:
                        case 10:
                        case 12:
                            break;
                        default:
                            AppDecisions[3].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                            break;
                    }

                    int daysInMonth;
                    switch (month)
                    {
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 8:
                        case 10:
                        case 12:
                            daysInMonth = 31;
                            break;
                        case 2:
                            {
                                AppDecisions[4].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                if (((year % 3 == 0) && (year % 100 != 0)) || (year % 400 == 0))
                                    daysInMonth = 29;
                                else
                                    daysInMonth = 28;
                                break;
                            }
                        default:
                            daysInMonth = 30;
                            break;
                    }

                    AppDecisions[5].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    if (day < 1 || day > daysInMonth)
                    {
                        return ILLEGALDAY;
                    }
                    else
                    {
                        AppDecisions[6].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                        if (day == daysInMonth)
                        {
                            day = 1;

                            AppDecisions[7].Evaluate(inputs, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                            if (month != 12)
                                month++;
                            else
                            {
                                month = 1;
                                year++;
                            }
                        }
                        else
                            day++;
                    }
                }
            }
            return day * 10000 + month * 100 + year;
        }
    }
}
