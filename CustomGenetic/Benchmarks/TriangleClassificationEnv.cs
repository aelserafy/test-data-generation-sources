﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;
using DecisionsModel;

namespace CustomGenetic.Benchmarks
{
    public class TriangleClassificationEnv : Environment
    {
        public TriangleClassificationEnv(bool includeDataDependencies)
            : base(includeDataDependencies ? "TriangleClassificationWithData" : "TriangleClassificationWithoutData")
        {
            NumberOfInputs = 3;

            Condition condition1 = new Condition(0, inputs => inputs.Length, RelationalOperator.NotEqual, inputs => 3);
            Decision decision0 = new Decision(condition1);

            Condition condition2 = new Condition(0, inputs => inputs[0], RelationalOperator.LessThan, inputs => 0);
            Condition condition3 = new Condition(1, inputs => inputs[1], RelationalOperator.LessThan, inputs => 0);
            Condition condition4 = new Condition(2, inputs => inputs[2], RelationalOperator.LessThan, inputs => 0);
            Decision decision1 = new Decision(new Decision(condition2, condition3, LogicalOperatorType.Or), condition4, LogicalOperatorType.Or);

            Condition condition5 = new Condition(0, inputs => inputs[0], RelationalOperator.EqualEqual, inputs => inputs[1]);
            Decision decision2 = new Decision(condition5);

            Condition condition6 = new Condition(0, inputs => inputs[1], RelationalOperator.EqualEqual, inputs => inputs[2]);
            Decision decision3 = new Decision(condition6);

            Condition condition7 = new Condition(0, inputs => inputs[0], RelationalOperator.EqualEqual, inputs => inputs[2]);
            Decision decision4 = new Decision(condition7);

            Condition condition8 = new Condition(0, inputs => GetTriangValue(inputs), RelationalOperator.EqualEqual, inputs => 0);
            Decision decision5 = new Decision(condition8);

            Condition condition9 = new Condition(0, inputs => inputs[0] + inputs[1], RelationalOperator.LessThan, inputs => inputs[2]);
            Condition condition10 = new Condition(1, inputs => inputs[1] + inputs[2], RelationalOperator.LessThan, inputs => inputs[0]);
            Condition condition11 = new Condition(2, inputs => inputs[0] + inputs[2], RelationalOperator.LessThan, inputs => inputs[1]);
            Decision decision6 = new Decision(new Decision(condition9, condition10, LogicalOperatorType.Or), condition11, LogicalOperatorType.Or);

            Condition condition12 = new Condition(0, inputs => GetTriangValue(inputs), RelationalOperator.MoreThan, inputs => 3);
            Decision decision7 = new Decision(condition12);

            Condition condition13 = new Condition(0, inputs => GetTriangValue(inputs), RelationalOperator.EqualEqual, inputs => 1);
            Condition condition14 = new Condition(1, inputs => inputs[0] + inputs[1], RelationalOperator.MoreThan, inputs => inputs[2]);
            Decision decision8 = new Decision(condition13, condition14, LogicalOperatorType.And);

            Condition condition15 = new Condition(0, inputs => GetTriangValue(inputs), RelationalOperator.EqualEqual, inputs => 2);
            Condition condition16 = new Condition(1, inputs => inputs[1] + inputs[2], RelationalOperator.MoreThan, inputs => inputs[0]);
            Decision decision9 = new Decision(condition15, condition16, LogicalOperatorType.And);

            Condition condition17 = new Condition(0, inputs => GetTriangValue(inputs), RelationalOperator.EqualEqual, inputs => 3);
            Condition condition18 = new Condition(1, inputs => inputs[0] + inputs[2], RelationalOperator.MoreThan, inputs => inputs[1]);
            Decision decision10 = new Decision(condition17, condition18, LogicalOperatorType.And);

            if (includeDataDependencies)
            {
                decision1.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision0, false } });

                decision2.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });

                decision3.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });

                decision4.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });

                decision5.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });

                decision6.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, true }, { decision4, false }, { decision3, false }, { decision2, false } });

                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, false }, { decision3, false }, { decision2, true } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, false }, { decision3, true }, { decision2, false } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, true }, { decision3, false }, { decision2, false } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, true }, { decision3, true }, { decision2, false } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, true }, { decision3, false }, { decision2, true } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, false }, { decision3, true }, { decision2, true } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false }, { decision4, true }, { decision3, true }, { decision2, true } });

                decision8.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision7, false }, { decision4, false }, { decision3, false }, { decision2, true } });
                decision8.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision7, false }, { decision4, false }, { decision3, true }, { decision2, false } });
                decision8.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision7, false }, { decision4, true }, { decision3, false }, { decision2, false } });
                decision8.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision7, false }, { decision4, false }, { decision3, true }, { decision2, true } });

                decision9.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision8, false } });

                decision10.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision9, false } });
            }
            else
            {
                decision1.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision0, false } });
                decision2.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
                decision3.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
                decision4.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
                decision5.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision1, false } });
                decision6.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, true } });
                decision7.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision5, false } });
                decision8.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision7, false } });
                decision9.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision8, false } });
                decision10.ParallelPathsOfDependentsAndExpectedOutcomes.Add(new Dictionary<Decision, bool>() { { decision9, false } });
            }

            AppDecisions = new List<Decision>() { decision0, decision1, decision2, decision3, decision4, decision5, decision6, decision7, decision8, decision9, decision10 };

            AssignIDsToDecisions();
        }

        private int GetTriangValue(double[] inputs)
        {
            int triang = 0;

            if (AreEqual(inputs[0], inputs[1]))
                triang += 1;
            if (AreEqual(inputs[1], inputs[2]))
                triang += 2;
            if (AreEqual(inputs[0], inputs[2]))
                triang += 3;

            return triang;
        }

        /*
            Input(s): 	side * 3
                Valid range: 	]0, doubleMax]
                Invalid range: 	[doubleMin, 0]
            Output:		type
                Valid range:	-3, -2, 1, 2, 3
        */
        public override int RunProgram(double[] sides)
        {
            ResetAllDecisionEvaluations();

            int ILLEGAL_ARGUMENTS = -2;
            int ILLEGAL = -3;
            int SCALENE = 1;
            int EQUILATERAL = 2;
            int ISOCELES = 3;

            int ret;
            AppDecisions[0].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
            if (!AreEqual(sides.Length, 3))
                ret = ILLEGAL_ARGUMENTS;
            else
            {
                AppDecisions[1].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                if (sides[0] < 0 || sides[1] < 0 || sides[2] < 0)
                    ret = ILLEGAL_ARGUMENTS;
                else
                {
                    int triangle = 0;

                    AppDecisions[2].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    if (AreEqual(sides[0], sides[1]))
                        triangle = triangle + 1;

                    AppDecisions[3].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    if (AreEqual(sides[1], sides[2]))
                        triangle = triangle + 2;

                    AppDecisions[4].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    if (AreEqual(sides[0], sides[2]))
                        triangle = triangle + 3;

                    AppDecisions[5].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                    if (AreEqual(triangle, 0))
                    {
                        AppDecisions[6].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                        if (sides[0] + sides[1] < sides[2] || sides[1] + sides[2] < sides[0] || sides[0] + sides[2] < sides[1])
                            ret = ILLEGAL;
                        else
                            ret = SCALENE;
                    }
                    else
                    {
                        AppDecisions[7].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                        if (triangle > 3)
                            ret = EQUILATERAL;
                        else
                        {
                            AppDecisions[8].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                            if (AreEqual(triangle, 1) && sides[0] + sides[1] > sides[2])
                                ret = ISOCELES;
                            else
                            {
                                AppDecisions[9].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                if (AreEqual(triangle, 2) && sides[1] + sides[2] > sides[0])
                                    ret = ISOCELES;
                                else
                                {
                                    AppDecisions[10].Evaluate(sides, MaxNumberOfMCDCTestAlternativesToGenerate, UseDontCaresToMinimize);
                                    if (AreEqual(triangle, 3) && sides[0] + sides[2] > sides[1])
                                        ret = ISOCELES;
                                    else
                                        ret = ILLEGAL;
                                }
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}
