﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using DecisionsModel;
using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;
using System;
using AForge;

namespace CustomGenetic
{
    public abstract class Environment : EqualityCompartor
    {
        public int ThreadID { get; private set; }
        public string BenchmarkName { get; private set; }
        private static ThreadSafeRandom rnd = new ThreadSafeRandom();
        public Environment(string benchMarkName)
        {
            BenchmarkName = benchMarkName;
            ThreadID = rnd.Next();
        }

        public int MaxNumberOfMCDCTestAlternativesToGenerate { get; private set; }

        public int PopulationSize { get; private set; }
        public double CrossoverRate { get; private set; }
        public double MutationRate { get; private set; }
        public double RandomSelectionPortion { get; private set; }
        public ISelectionMethod Selection { get; private set; }

        private int _GAGenerationsPerEqualityPredicate { get; set; }
        public int GAGenerationsPerEqualityPredicate
        {
            get { return RandomizePopulationIfStuck ? _GAGenerationsPerEqualityPredicate * 2 : _GAGenerationsPerEqualityPredicate; }
            private set { _GAGenerationsPerEqualityPredicate = value; }
        }
        private int _GAGenerationsPerNonEqualityPredicate { get; set; }
        public int GAGenerationsPerNonEqualityPredicate
        {
            get { return RandomizePopulationIfStuck ? _GAGenerationsPerNonEqualityPredicate * 2 : _GAGenerationsPerNonEqualityPredicate; }
            private set { _GAGenerationsPerNonEqualityPredicate = value; }
        }

        public double InputMax { get; private set; }
        public double InputMin { get; private set; }
        public bool UseDoubles { get; private set; }

        public double LeastIncrementalBoundaryConstant
        {
            get { return UseDoubles ? 0.01 : 1; }
        }
        public double BDConstant
        {
            get
            {
                return UseMessedUpBDFitness ? 1 : UseDoubles ? 0.01 : 1;
            }
        }
        public bool UseMessedUpBDFitness { get; private set; }
        public bool UseDontCaresToMinimize { get; private set; }
        public bool ConsiderDontCaresAndOutputValues { get; private set; }
        public bool RandomizePopulationIfStuck { get; private set; }
        public bool HeadStartIndividualsOnPopulationInit { get; private set; }

        public double MutationRateIncrease { get; private set; }
        private bool EnableMutationIncreaseOverIterations { get; set; }

        private bool UseDynamicGenerationsCountLimit { get; set; }

        public int GAGenerationsLimit
        {
            get { return UseDynamicGenerationsCountLimit ? TargetDecisionGAGenerationsLimit : 5000; }
        }

        public Decision TargetDecision
        {
            set
            {
                TargetDecisionGAGenerationsLimit = CalculateNumberOfGAGenerationsRequired(value);
                UpdateMutationRateIncrease();
            }
        }

        private int TargetDecisionGAGenerationsLimit { get; set; }

        public int NumberOfInputs { get; protected set; }
        public List<Decision> AppDecisions { get; protected set; }

        public abstract int RunProgram(double[] inputs);

        public void SetRunConfigurations(bool useMessedUpBDFitness, bool useDontCaresToMinimize, bool useDynamicGenerationsCountLimit, bool considerDontCaresAndOutputValues)
        {
            MaxNumberOfMCDCTestAlternativesToGenerate = 20;
            UseMessedUpBDFitness = useMessedUpBDFitness;
            UseDontCaresToMinimize = useDontCaresToMinimize;
            UseDynamicGenerationsCountLimit = useDynamicGenerationsCountLimit;
            ConsiderDontCaresAndOutputValues = considerDontCaresAndOutputValues;
        }

        public void SetGAConfigurations(int populationSize, double crossoverRate, double mutationRate, ISelectionMethod selection, double randomSelectionPortion, bool enableMutationIncreaseOverIterations, bool randomizePopulationIfStuck, bool headStartIndividualsOnPopulationInit)
        {
            PopulationSize = populationSize;
            CrossoverRate = crossoverRate;
            MutationRate = mutationRate;
            Selection = selection;
            RandomSelectionPortion = randomSelectionPortion;
            RandomizePopulationIfStuck = randomizePopulationIfStuck;
            HeadStartIndividualsOnPopulationInit = headStartIndividualsOnPopulationInit;
            EnableMutationIncreaseOverIterations = enableMutationIncreaseOverIterations;
            UpdateMutationRateIncrease();
        }

        public void SetTargetDataType(bool useDoubles)
        {
            if (useDoubles)
                SetTargetDataType(true, long.MaxValue, long.MinValue);
            else
                SetTargetDataType(false, int.MaxValue, int.MinValue);
        }

        public void SetTargetDataType(bool useDoubles, double inputMax, double inputMin)
        {
            UseDoubles = useDoubles;
            InputMax = inputMax;
            InputMin = inputMin;
            if (useDoubles)
                GAGenerationsPerEqualityPredicate = 20000;
            else
                GAGenerationsPerEqualityPredicate = 5000;
            GAGenerationsPerNonEqualityPredicate = 200;
        }

        private void UpdateMutationRateIncrease()
        {
            if (EnableMutationIncreaseOverIterations)
                MutationRateIncrease = (1 - MutationRate) / (GAGenerationsLimit - 1);
            else
                MutationRateIncrease = 0;
        }

        protected void ResetAllDecisionEvaluations()
        {
            for (int i = 0; i < AppDecisions.Count; i++)
                AppDecisions[i].EvaluatedTruthTableRow = null;
        }

        protected void AssignIDsToDecisions()
        {
            for (int i = 0; i < AppDecisions.Count; i++)
                AppDecisions[i].ID = i;
        }

        public int CalculateNumberOfGAGenerationsRequired(Decision decision)
        {
            int maxNumberOfGenerations = 0;
            foreach (Dictionary<Decision, bool> possiblePath in decision.ParallelPathsOfDependentsAndExpectedOutcomes)
            {
                int pathRequiredGAGenerationsCount = 0;
                for (int j = 0; j < possiblePath.Keys.Count; j++)
                    pathRequiredGAGenerationsCount += CalculateNumberOfGAGenerationsRequired(possiblePath.Keys.ElementAt(j));
                if (pathRequiredGAGenerationsCount > maxNumberOfGenerations)
                    maxNumberOfGenerations = pathRequiredGAGenerationsCount;
            }
            int equalEqualPredicatesCount = 0, nonEqualEqualPredicatesCount = 0;
            decision.GetEqualEqualAndNonEqualEqualPredicatesCount(ref equalEqualPredicatesCount, ref nonEqualEqualPredicatesCount);
            return GAGenerationsPerEqualityPredicate * equalEqualPredicatesCount + GAGenerationsPerNonEqualityPredicate * nonEqualEqualPredicatesCount + maxNumberOfGenerations;
        }
    }
}
