﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using ZedGraph;

namespace CustomGenetic
{
    public partial class Form1 : Form
    {
        double[] BestSolutionOverIterations;
        public Form1(double[] bestSolutionOverIterations)
        {
            BestSolutionOverIterations = bestSolutionOverIterations;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            z1.GraphPane.Title = "Best solution fitness over iterations";

            double[] x = new double[BestSolutionOverIterations.Length];
            double[] y = new double[BestSolutionOverIterations.Length];

            for (int i = 0; i < BestSolutionOverIterations.Length; i++)
            {
                x[i] = i;
                y[i] = BestSolutionOverIterations[i];
            }
            z1.GraphPane.AddCurve("Sine Wave", x, y, Color.Red);
            z1.AxisChange();
            z1.Invalidate();
        }
    }
}
