﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using AForge.Genetic;
using AForge;

namespace CustomGenetic.GAOperators
{
    public static class RandomExtensions
    {
        public static double NextDouble(this ThreadSafeRandom random, double minValue, double maxValue)
        {
            return random.NextDouble() * (maxValue - minValue) + minValue;
        }
    }

    class MyChromosome : ChromosomeBase
    {
        private Environment Env;
        public double[] InputValuesArray { get; private set; }

        public MyChromosome(Environment env)
            : this(env, null)
        {
        }

        public MyChromosome(Environment env, double[] inputValuesArray)
        {
            Env = env;
            InputValuesArray = new double[Env.NumberOfInputs];
            if (null != inputValuesArray)
            {
                if (inputValuesArray.Length != env.NumberOfInputs)
                    throw new Exception("Initializing a chromosome with a different number of inputs from the environment");
                for (int i = 0; i < inputValuesArray.Length; i++)
                    InputValuesArray[i] = inputValuesArray[i];
            }

        }

        public override void Generate()
        {
            for (int i = 0; i < InputValuesArray.Length; i++)
                InputValuesArray[i] = GenerateNextRandom();
        }

        public IChromosome NegateValues()
        {
            for (int i = 0; i < InputValuesArray.Length; i++)
                InputValuesArray[i] = -InputValuesArray[i];
            return this;
        }

        private double GenerateNextRandom()
        {
            try
            {
                ThreadSafeRandom rnd = new ThreadSafeRandom();
                if (Env.UseDoubles)
                    return rnd.NextDouble(Env.InputMin, Env.InputMax);
                else
                    return rnd.Next((int)Env.InputMin, (int)Env.InputMax);
            }
            catch (Exception ex)
            {
                Logger.Log(MsgType.Exception, Env.ThreadID, Env.BenchmarkName, "Err while generating a rand #, plz check that u've put the proper range bounds" + System.Environment.NewLine + ex.ToString());
            }
            return 0;
        }

        public override IChromosome CreateNew()
        {
            MyChromosome newChromsome = new MyChromosome(Env);
            newChromsome.Generate();
            return newChromsome;
        }

        public override IChromosome Clone()
        {
            MyChromosome newChromsome = new MyChromosome(Env);
            for (int i = 0; i < InputValuesArray.Length; i++)
                newChromsome.InputValuesArray[i] = InputValuesArray[i];
            newChromsome.fitness = fitness;
            return newChromsome;
        }

        public override void Mutate()
        {
            ThreadSafeRandom rnd = new ThreadSafeRandom();
            for (int i = 0; i < InputValuesArray.Length; i++)
                if (rnd.NextDouble() <= Env.MutationRate)
                    InputValuesArray[i] = GenerateNextRandom();
        }

        public override void Crossover(IChromosome pair)
        {
            MyChromosome castedPair = (MyChromosome)pair;
            for (int i = 0; i < InputValuesArray.Length; i++)
            {
                double[] genesValues = new double[] { InputValuesArray[i], castedPair.InputValuesArray[i] };
                ThreadSafeRandom rnd = new ThreadSafeRandom();
                double alpha = rnd.NextDouble(0, 1);
                InputValuesArray[i] = alpha * genesValues[0] + (1 - alpha) * genesValues[1];
                castedPair.InputValuesArray[i] = (1 - alpha) * genesValues[0] + alpha * genesValues[1];

                if (!Env.UseDoubles)
                {
                    InputValuesArray[i] = Math.Round(InputValuesArray[i]);
                    castedPair.InputValuesArray[i] = Math.Round(castedPair.InputValuesArray[i]);
                }
            }
        }
    }
}
