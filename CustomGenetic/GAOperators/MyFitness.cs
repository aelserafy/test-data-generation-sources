﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;
using DecisionsModel;

namespace CustomGenetic.GAOperators
{
    delegate int RunProgram(double[] inputs);
    public class MyFitness : IFitnessFunction
    {
        Decision TargetDecision { get; set; }
        TruthTableRow TargetTestCase { get; set; }
        bool UseDontCaresToMinimize { get; set; }
        bool UseMessedUpBDFitness { get; set; }
        double BranchDistanceConstant { get; set; }
        bool ConsiderDontCaresAndOutputValues { get; set; }
        RunProgram RunProgram { get; set; }
        int ThreadID { get; set; }
        string BenchmarkName { get; set; }

        public MyFitness(Environment env, Decision targetDecision, TruthTableRow targetTestCase)
        {
            UseDontCaresToMinimize = env.UseDontCaresToMinimize;
            UseMessedUpBDFitness = env.UseMessedUpBDFitness;
            BranchDistanceConstant = env.BDConstant;
            ConsiderDontCaresAndOutputValues = env.ConsiderDontCaresAndOutputValues;
            TargetDecision = targetDecision;
            TargetTestCase = targetTestCase;
            RunProgram = env.RunProgram;
            ThreadID = env.ThreadID;
            BenchmarkName = env.BenchmarkName;
        }

        public double Evaluate(IChromosome chromosome)
        {
            return Evaluate(((MyChromosome)chromosome).InputValuesArray);
        }

        public double Evaluate(int[] inputs)
        {
            double[] doubleInputs = new double[inputs.Length];
            for (int i = 0; i < inputs.Length; i++)
                doubleInputs[i] = inputs[i];
            return Evaluate(doubleInputs);
        }

        public double Evaluate(double[] inputs)
        {
            double calcedFitness = EvaluateInputDataFitness(inputs);
            return 1 / calcedFitness; // because it's a minimization problem while AForge's framework is trying to maximize
        }

        private double EvaluateInputDataFitness(double[] inputs)
        {
            try
            {
                double minFitness = double.MaxValue;
                if (null == TargetDecision || null == TargetTestCase)
                    return minFitness;

                RunProgram(inputs);
                if (!UseDontCaresToMinimize)
                {
                    int ApproachLevel = 0; 
                    double BranchDistance = 0;
                    Evaluate(TargetDecision, TargetDecision.DecisionTruthTable[TargetTestCase.RowIndex].Output, ref ApproachLevel, ref BranchDistance, inputs);
                    minFitness = ApproachLevel + BranchDistance;
                }
                else
                {
                    TruthTableRow originalTestCase = TargetTestCase;
                    for (int i = 0; i < TargetDecision.DecisionTruthTable.Length; i++)
                    {
                        if (originalTestCase.IsTruthTableRowEquivalentConsideringDontCares(TargetDecision.DecisionTruthTable[i], ConsiderDontCaresAndOutputValues))
                        {
                            int ApproachLevel = 0;
                            double BranchDistance = 0;
                            TargetTestCase = TargetDecision.DecisionTruthTable[i];
                            Evaluate(TargetDecision, TargetDecision.DecisionTruthTable[i].Output, ref ApproachLevel, ref BranchDistance, inputs);
                            if (ApproachLevel + BranchDistance < minFitness)
                                minFitness = ApproachLevel + BranchDistance;
                        }
                    }
                    TargetTestCase = originalTestCase;
                }
                return minFitness;
            }
            catch (NullReferenceException ex)
            {
                string errorMsg = "Null Ref. on inputs: ";
                foreach (double input in inputs)
                    errorMsg += input + ",";
                errorMsg = errorMsg.TrimEnd(',') + System.Environment.NewLine;
                errorMsg += "Target decision ID: " + TargetDecision.ID + System.Environment.NewLine;
                errorMsg += "Target test case row ID: " + TargetTestCase.RowIndex + System.Environment.NewLine;
                Logger.Log(MsgType.Exception, ThreadID, BenchmarkName, errorMsg + System.Environment.NewLine + ex.ToString());
                return double.MaxValue;
            }
        }

        private void Evaluate(Decision decision, bool desiredOutcome, ref int ApproachLevel, ref double BranchDistance, double[] inputs)
        {
            int minApproachLevel = 0;
            double minBranchDistance = 0;
            for (int i = 0; i < decision.ParallelPathsOfDependentsAndExpectedOutcomes.Count; i++)
            {
                Dictionary<Decision, bool> seriesOfDependentDecisionsAndOutcomes = decision.ParallelPathsOfDependentsAndExpectedOutcomes[i];
                int backUpAL = ApproachLevel;
                double backUpBD = BranchDistance;
                backUpAL += seriesOfDependentDecisionsAndOutcomes.Keys.Count;
                for (int j = 0; j < seriesOfDependentDecisionsAndOutcomes.Keys.Count; j++)
                    Evaluate(seriesOfDependentDecisionsAndOutcomes.Keys.ElementAt(j), seriesOfDependentDecisionsAndOutcomes.Values.ElementAt(j), ref backUpAL, ref backUpBD, inputs);

                if (0 == i)
                {
                    minApproachLevel = backUpAL;
                    minBranchDistance = backUpBD;
                }
                else if (backUpAL + backUpBD < minApproachLevel + minBranchDistance)
                {
                    minApproachLevel = backUpAL;
                    minBranchDistance = backUpBD;
                }
            }
            if (decision.ParallelPathsOfDependentsAndExpectedOutcomes.Count > 0)
            {
                ApproachLevel = minApproachLevel;
                BranchDistance = minBranchDistance;
            }

            if (ApproachLevel == 0)
            {
                if (!UseDontCaresToMinimize)
                {
                    if (!decision.EvaluatedTruthTableRow.Equals(TargetTestCase))
                    {
                        double calcedBD = decision.CalcBranchDistance(desiredOutcome, TargetTestCase, inputs, BranchDistanceConstant, UseMessedUpBDFitness);
                        BranchDistance = calcedBD / (calcedBD + 1);
                    }
                    else
                        BranchDistance = 0;
                }
                else
                {
                    if (!TargetTestCase.IsTruthTableRowEquivalentConsideringDontCares(decision.EvaluatedTruthTableRow, ConsiderDontCaresAndOutputValues))
                    {
                        double calcedBD = decision.CalcBranchDistance(desiredOutcome, TargetTestCase, inputs, BranchDistanceConstant, UseMessedUpBDFitness);
                        BranchDistance = calcedBD / (calcedBD + 1);
                    }
                    else
                        BranchDistance = 0;
                }
                return;
            }

            if (decision.EvaluatedTruthTableRow != null)
            {
                if (desiredOutcome != decision.EvaluatedTruthTableRow.Output)
                {
                    double calcedBD = decision.CalcBranchDistance(desiredOutcome, null, inputs, BranchDistanceConstant, UseMessedUpBDFitness);
                    BranchDistance += calcedBD / (calcedBD + 1);
                }
                else
                    ApproachLevel--;
            }
        }
    }
}
