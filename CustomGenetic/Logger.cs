﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using AForge;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
namespace CustomGenetic
{
    public enum MsgType { Info, Progress, Exception, NewEvaluation };
    public class Logger
    {
        private static Queue<string> LogMsgs;
        private static Logger instance = new Logger();
        private static bool KeepLoggerAlive;
        private static string LogFileName;
        private static string MachineName;
        private static ThreadSafeRandom rnd = new ThreadSafeRandom();
        private static readonly int NumberOfRetries = 1000;
        private static readonly int SleepPeriod = 30000;
        private static string SafeFallbackErrorFile;

        static Logger()
        { }

        private Logger()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(ThreadExceptionHandlingMethod); //For UI threads
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandlingMethod); //For non-UI threads
            LogMsgs = new Queue<string>();
            KeepLoggerAlive = true;
            MachineName = System.Environment.MachineName;
            LogFileName = "log.csv";
            SafeFallbackErrorFile = "error.txt";
            Log(MsgType.Info, 0, "Initiating Logger");
            Thread log = new Thread(() => LoggingThread());
            log.Start();
        }

        public static void Log(MsgType type, int threadID, string benchmarkName, string msgText)
        {
            Log(type, threadID, benchmarkName + "," + msgText);
        }

        private static void Log(MsgType type, int threadID, string msgText)
        {
            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff (zz)");
            string allMsg = time + "," + MachineName + "," + threadID.ToString() + "," + type.ToString() + "," + msgText + System.Environment.NewLine;
            lock (LogMsgs)
            {
                LogMsgs.Enqueue(allMsg);
            }
        }

        public static void Kill()
        {
            KeepLoggerAlive = false;
        }

        ~Logger()
        {
            try
            {
                Kill();
            }
            catch(Exception ex)
            {
                string msg = "Error while killing logger!!" + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine;
                try
                {
                    File.AppendAllText(SafeFallbackErrorFile, msg);
                }
                catch (Exception ex1)
                {
                    Console.Error.WriteLine(msg);
                }
            }

        }

        private static void LoggingThread()
        {
            try
            {
                while (KeepLoggerAlive)
                {
                    WriteLogMsgs();
                    if (!KeepLoggerAlive && !(LogMsgs.Count > 0))
                        break;
                    Thread.Sleep(SleepPeriod);
                }
                Log(MsgType.Info, 0, "Logger stopped");
                WriteLogMsgs();
            }
            catch (Exception ex)
            {
                string msg = "Error while logging!!" + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine;
                try
                {
                    File.AppendAllText(SafeFallbackErrorFile, msg);

                }
                catch (Exception ex1)
                {
                    Console.Error.WriteLine(msg);
                }
            }
        }

        private static void WriteLogMsgs()
        {
            int numberOfRetries = NumberOfRetries;
            string accumlatedMsg = "";
            lock (LogMsgs)
            {
                while (LogMsgs.Count > 0)
                    accumlatedMsg += LogMsgs.Dequeue();
            }
            while (!string.IsNullOrEmpty(accumlatedMsg))
            {
                //if (numberOfRetries == 0)
                //    throw new Exception("Unable to write in logger");
                try
                {
                    File.AppendAllText(LogFileName, accumlatedMsg);
                    accumlatedMsg = "";
                }
                catch (IOException ex)
                {
                    numberOfRetries--;
                    Thread.Sleep(SleepPeriod * rnd.Next(0, 1));
                }
            }
        }

        public static void UnhandledExceptionHandlingMethod(object sender, UnhandledExceptionEventArgs t)
        {
            try
            {
                Log(MsgType.Exception, 0, t.ToString());
            }
            catch(Exception ex)
            {
                string msg = "Error on logging Unhandled exception!!" + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine + t.ToString() + System.Environment.NewLine;
                try
                {
                    File.AppendAllText(SafeFallbackErrorFile, msg);

                }
                catch (Exception ex1)
                {
                    Console.Error.WriteLine(msg);
                }
            }
        }

        public static void ThreadExceptionHandlingMethod(object sender, ThreadExceptionEventArgs t)
        {
            try
            {
                Log(MsgType.Exception, 0, t.ToString());
            }
            catch(Exception ex)
            {
                string msg = "Error on logging thread exception!!" + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine + t.ToString() + System.Environment.NewLine;
                try
                {
                    File.AppendAllText(SafeFallbackErrorFile, msg);

                }
                catch (Exception ex1)
                {
                    Console.Error.WriteLine(msg);
                }
            }
        }
    }
}
