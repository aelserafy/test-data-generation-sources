﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using AForge.Genetic;
using DecisionsModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using CustomGenetic.Benchmarks;
using CustomGenetic.GAOperators;
using CustomGenetic.SolutionRepresentation;
using DecisionsModel.TestTargetsGenerators;
using AForge;

namespace CustomGenetic
{
    class Program
    {
        static int numberOfCoresToUtilize;
        //args[0]: output file path
        //args[1]: number of GA evaluations
        //args[2]: target benchmark
        //args[3]: max input range
        //args[4]: CPU load %
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                string fileOutput = "Original # of MC/DC targets, Original coverage, Original generations count, Original effective generations, Optimized # of test data of the original approach,"; //5 columns
                fileOutput += "Our # of MC/DC targets, Our coverage, Our generations count, Our effective generations, Optimized # of test data of our approach,"; //5 columns
                fileOutput += "# of BVC targets, Satisfied BVC targets percentage, # of satisfied BVC targets from MC/DC, # of extra generations to cover BVC, BVC effective generations, Optimized # of BVC test data,"; //6 columns
                //fileOutput += "Selection Coverage, Selection generations, 0.25 population rand Coverage, 0.25 population rand generations,"; //4 columns
                //fileOutput += "Mutation Coverage, Mutation generations, Previous 3 Coverage, Previous 3 Iterations,"; //4 columns
                //fileOutput += "BD Coverage, BD generations, X Coverage, X generations, Generations Coverage, Generations generations"; //6 columns
                fileOutput += System.Environment.NewLine;

                string fileName = args[0] + "_" + args[2] + ".csv";
                if (!File.Exists(fileName))
                    File.WriteAllText(fileName, fileOutput);

                int statIterationsCount = int.Parse(args[1]);
                List<Thread> runningThreads = new List<Thread>();
                for (int stat = 0; stat < statIterationsCount; stat++)
                {
                    numberOfCoresToUtilize = Math.Max(1, (int)(System.Environment.ProcessorCount * double.Parse(File.ReadAllText("CPU_Load.txt"))));
                    Environment[] envs = SetupEnvsForNewThread(args);
                    Thread evaluation = new Thread(() => RunEvaluation(fileName, envs));
                    evaluation.Priority = ThreadPriority.Highest;
                    evaluation.Start();
                    runningThreads.Add(evaluation);

                    while (runningThreads.Count == numberOfCoresToUtilize)
                        RemoveThreadWhenDone(runningThreads);
                }
                while (runningThreads.Count > 0)
                    RemoveThreadWhenDone(runningThreads);
            }

            catch (Exception ex)
            {
                Logger.Log(MsgType.Exception, 0, "Main", ex.ToString());
            }
            finally
            {
                Logger.Kill();
            }
        }

        private static void RemoveThreadWhenDone(List<Thread> runningThreads)
        {
            int j = 0;
            while (j < runningThreads.Count)
                if (!runningThreads[j].IsAlive)
                {
                    runningThreads.RemoveAt(j);
                    break;
                }
                else
                    j++;
        }

        private static void RunEvaluation(string fileName, Environment[] envs)
        {
            try
            {
                string[] fileOutputStrings = new string[5 + 5 + 6 + 4 * 2 + 6];
                for (int x = envs.Length - 1; x >= 0; x--)
                {
                    Environment env = envs[x];
                    if (env is NextDateEnvWithIf && ((NextDateEnvWithIf)env).UseDoubleVersion && 0 == x)
                        continue;
                    Logger.Log(MsgType.Info, env.ThreadID, env.BenchmarkName, "Start_env" + x);
                    switch (x)
                    {
                        case 0:
                            env.SetRunConfigurations(true, false, false, true);
                            env.SetGAConfigurations(100, 0.7, 0.05, new EliteSelection(), 0.9, false, false, false);
                            break;
                        case 1:
                            env.SetRunConfigurations(false, true, true, true);
                            env.SetGAConfigurations(100, 0.7, 0.05, new ElitistRankingSelection(), 0.25, true, true, true);
                            break;
                        default:
                            throw new Exception("This is not expected while initializing environments");
                    }

                    Solutions mcdcSolutions = new Solutions(env, null);

                    for (int i = 0; i < env.AppDecisions.Count; i++)
                    {
                        Logger.Log(MsgType.Info, env.ThreadID, env.BenchmarkName, "env" + x + "_MCDC_" + i + "/" + (env.AppDecisions.Count - 1));
                        List<PossibleMCDCCombination> possibleTargetMCDCCombinations = env.AppDecisions[i].GenerateMCDCTestAlternatives(env.MaxNumberOfMCDCTestAlternativesToGenerate, env.UseDontCaresToMinimize);

                        List<Solutions> mcdcCombinationsSolutions = new List<Solutions>();

                        for (int l = 0; l < possibleTargetMCDCCombinations.Count; l++)
                        {
                            Solutions currentSolution = new Solutions(env, possibleTargetMCDCCombinations[l]);
                            mcdcCombinationsSolutions.Add(currentSolution);
                            currentSolution.NumberOfTestTargets = possibleTargetMCDCCombinations[l].MCDCRows.Count;
                            for (int j = 0; j < possibleTargetMCDCCombinations[l].MCDCRows.Count; j++)
                            {
                                IChromosome originChromosome = null;// new MyChromosome(env);
                                if (env is EqualityTestEnv && j == 0)
                                    continue;
                                IChromosome bestSolution = null;

                                bool useDoubles = env.UseDoubles;
                                env.SetTargetDataType(false);
                                bool foundSolution = FindSolution(env, currentSolution, env.AppDecisions[i], possibleTargetMCDCCombinations[l].MCDCRows[j], possibleTargetMCDCCombinations[l], null, originChromosome, out bestSolution);
                                env.SetTargetDataType(useDoubles);

                                if (env.UseDoubles && !foundSolution)
                                    FindSolution(env, currentSolution, env.AppDecisions[i], possibleTargetMCDCCombinations[l].MCDCRows[j], possibleTargetMCDCCombinations[l], null, bestSolution, out bestSolution);
                            }
                            if (EqualityCompartor.StaticAreEqual(100, currentSolution.SatisfactionPercentage))
                                break;
                        }
                        mcdcCombinationsSolutions.Sort();
                        mcdcSolutions.UpdateWithTheseSolutions(mcdcCombinationsSolutions[0], env.AppDecisions[i]);
                    }
                    int optimizedMCDCTestSuiteSize = mcdcSolutions.OptimizedTestSuiteSize;

                    switch (x)
                    {
                        case 0:
                            fileOutputStrings[0] = mcdcSolutions.NumberOfTestTargets.ToString();
                            fileOutputStrings[1] = mcdcSolutions.SatisfactionPercentage.ToString();
                            fileOutputStrings[2] = mcdcSolutions.GenerationsConsumed.ToString();
                            fileOutputStrings[3] = mcdcSolutions.EffectiveGenerationsPercentage.ToString();
                            fileOutputStrings[4] = optimizedMCDCTestSuiteSize.ToString();
                            break;
                        case 1:
                            fileOutputStrings[5] = mcdcSolutions.NumberOfTestTargets.ToString();
                            fileOutputStrings[6] = mcdcSolutions.SatisfactionPercentage.ToString();
                            fileOutputStrings[7] = mcdcSolutions.GenerationsConsumed.ToString();
                            fileOutputStrings[8] = mcdcSolutions.EffectiveGenerationsPercentage.ToString();
                            fileOutputStrings[9] = optimizedMCDCTestSuiteSize.ToString();
                            break;
                        default:
                            throw new Exception("This is not expected while writting results");
                    }

                    if (1 == x)
                    {
                        env.SetRunConfigurations(false, true, true, false);
                        Solutions bvcSolutions = new Solutions(env, null);
                        for (int i = 0; i < env.AppDecisions.Count; i++)
                        {
                            Logger.Log(MsgType.Info, env.ThreadID, env.BenchmarkName, "env" + x + "_BVC_" + i + "/" + (env.AppDecisions.Count - 1));
                            List<ConditionBVCTarget> bvcTestTargets = env.AppDecisions[i].GenerateBVCTestTargets(env.LeastIncrementalBoundaryConstant, env.UseDontCaresToMinimize);

                            for (int j = 0; j < bvcTestTargets.Count; j++)
                            {
                                bvcSolutions.NumberOfTestTargets += bvcTestTargets[j].ReplacingConditions.Count;
                                Decision targetDecision = bvcTestTargets[j].Decsion;
                                int conditionIndex = bvcTestTargets[j].ConditionIndex;
                                for (int k = 0; k < bvcTestTargets[j].ReplacingConditions.Count; k++)
                                {
                                    targetDecision.ConditionsList[conditionIndex].Alter(bvcTestTargets[j].ReplacingConditions[k]);
                                    bool foundSolution = false;
                                    for (int l = 0; !foundSolution && l < bvcTestTargets[j].TruthTableRowsThatCauseThisConditionToEvaluate.Count; l++)
                                    {
                                        TruthTableRow targetTestCase = bvcTestTargets[j].TruthTableRowsThatCauseThisConditionToEvaluate[l];
                                        SingleTargetSolutionData solutionFromMCDC = mcdcSolutions.GetTestDataThatExecuteThisTestTarget(targetDecision, targetTestCase);
                                        foundSolution = null != solutionFromMCDC;
                                        if (foundSolution)
                                        {
                                            mcdcSolutions.NumberOfAchievedBVCTargetsFromMCDCSolutions = ++bvcSolutions.NumberOfAchievedBVCTargetsFromMCDCSolutions;
                                            bvcSolutions.NumberOfReachedTestTargets++;
                                            SingleTargetSolutionData solution = new SingleTargetSolutionData(new MyChromosome(env, solutionFromMCDC.Solution.InputValuesArray), targetDecision, targetTestCase, null, bvcTestTargets[j].ReplacingConditions[k]);
                                            int bvcSolutionIndex = bvcSolutions.SolutionsData.IndexOf(solution);
                                            if (bvcSolutionIndex < 0)
                                                bvcSolutions.SolutionsData.Add(solution);
                                            else
                                                bvcSolutions.SolutionsData[bvcSolutionIndex].AddTestTarget(targetDecision, targetTestCase, null, bvcTestTargets[j].ReplacingConditions[k]);
                                        }
                                        else
                                        {
                                            IChromosome bestSolution = null;
                                            bool useDoubles = env.UseDoubles;
                                            env.SetTargetDataType(false);
                                            foundSolution = FindSolution(env, bvcSolutions, targetDecision, targetTestCase, null, bvcTestTargets[j].ReplacingConditions[k], null, out bestSolution);
                                            env.SetTargetDataType(useDoubles);
                                            if (env.UseDoubles && !foundSolution)
                                            {
                                                //TODO: Disabled because it is taking so much time to search in the extended search space!
                                                foundSolution = FindSolution(env, bvcSolutions, targetDecision, targetTestCase, null, bvcTestTargets[j].ReplacingConditions[k], bestSolution, out bestSolution);
                                            }
                                        }
                                    }
                                }
                                targetDecision.ConditionsList[conditionIndex].RestoreOriginalCondition();
                            }
                        }
                        int optimizedBVCTestSuiteSize = bvcSolutions.OptimizedTestSuiteSize;

                        fileOutputStrings[10] = bvcSolutions.NumberOfTestTargets.ToString();
                        fileOutputStrings[11] = bvcSolutions.SatisfactionPercentage.ToString();
                        fileOutputStrings[12] = bvcSolutions.NumberOfAchievedBVCTargetsFromMCDCSolutions.ToString();
                        fileOutputStrings[13] = bvcSolutions.GenerationsConsumed.ToString();
                        fileOutputStrings[14] = bvcSolutions.EffectiveGenerationsPercentage.ToString();
                        fileOutputStrings[15] = optimizedBVCTestSuiteSize.ToString();
                    }
                    Logger.Log(MsgType.Info, env.ThreadID, env.BenchmarkName, "End_env" + x);
                }

                string fileOutput = "";
                for (int i = 0; i < fileOutputStrings.Length; i++)
                    fileOutput += fileOutputStrings[i] + ",";
                fileOutput += System.Environment.NewLine;

                while (!string.IsNullOrEmpty(fileOutput))
                {
                    try
                    {
                        File.AppendAllText(fileName, fileOutput);
                        fileOutput = "";
                    }
                    catch (IOException ex)
                    {
                        Thread.Sleep(1000 * new ThreadSafeRandom().Next(0, 1));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MsgType.Exception, 0, "RunEvaluation", ex.ToString());
            }
        }

        private static Environment[] SetupEnvsForNewThread(string[] args)
        {
            Environment[] envs = new Environment[2];

            for (int i = 0; i < envs.Length; i++)
            {
                if ("NextDateWithSwitch" == args[2])
                    envs[i] = new NextDateEnvWithSwitch();
                if ("NextDateWithIfInt" == args[2])
                    envs[i] = new NextDateEnvWithIf(false);
                if ("NextDateWithIfDouble" == args[2])
                    envs[i] = new NextDateEnvWithIf(true);
                else if ("TriangleClassificationWithData" == args[2])
                    envs[i] = new TriangleClassificationEnv(true);
                else if ("TriangleClassificationWithoutData" == args[2])
                    envs[i] = new TriangleClassificationEnv(false);
                else if ("EqualityGenerationsMeasurement" == args[2])
                    envs[i] = new EqualityTestEnv(true);
                else if ("InEqualityGenerationsMeasurement" == args[2])
                    envs[i] = new EqualityTestEnv(false);

                if ("int" == args[3])
                    envs[i].SetTargetDataType(false);
                else if ("double" == args[3])
                    envs[i].SetTargetDataType(true);
                else
                    envs[i].SetTargetDataType(false, int.Parse(args[3]), -envs[i].InputMax);
            }
            return envs;
        }

        private static bool FindSolution(Environment env, Solutions solutions, Decision decision, TruthTableRow testCase, PossibleMCDCCombination mcdcCombination, ConditionPossibleAlternation condition, IChromosome originChromosome, out IChromosome bestChromosome)
        {
            env.TargetDecision = decision;

            MyFitness fitness = new MyFitness(env, decision, testCase);
            Population population = InitializePopulation(env, originChromosome, fitness, solutions);

            bool foundSolution = false;

            double bestFitness = 0;
            int bestFitnessRepititionLimit = InitializeBestFitnessRepititionLimit(env);
            for (int i = 0; !foundSolution && i < env.GAGenerationsLimit; i++)
            {
                population.RunEpoch();
                population.MutationRate += env.MutationRateIncrease;

                if (bestFitness != population.BestChromosome.Fitness)
                {
                    bestFitness = population.BestChromosome.Fitness;
                    bestFitnessRepititionLimit = InitializeBestFitnessRepititionLimit(env);
                }
                else
                    bestFitnessRepititionLimit--;

                if (env.RandomizePopulationIfStuck && bestFitnessRepititionLimit == 0)
                {
                    double currentMutationRate = population.MutationRate;
                    population = InitializePopulation(env, population.BestChromosome, fitness, solutions);
                    population.MutationRate = currentMutationRate;
                    bestFitnessRepititionLimit = InitializeBestFitnessRepititionLimit(env);
                    continue;
                }

                if (double.IsInfinity(population.BestChromosome.Fitness))
                {
                    solutions.SolutionsData.Add(new SingleTargetSolutionData(population.BestChromosome, decision, testCase, mcdcCombination, condition));
                    solutions.NumberOfReachedTestTargets++;
                    solutions.GenerationsConsumed += i;
                    solutions.EffectiveGenerationsConsumed += i;
                    foundSolution = true;
                }
                //double[] bestSolutionOverIterations = new double[numberOfIterations];
                //bestSolutionOverIterations[k] = population.BestChromosome.Fitness;
                //Application.Run(new Form1(bestSolutionOverIterations));
            }

            if (!foundSolution)
                solutions.GenerationsConsumed += env.GAGenerationsLimit;
            bestChromosome = population.BestChromosome;
            return foundSolution;
        }

        private static int InitializeBestFitnessRepititionLimit(Environment env)
        {
            return env.GAGenerationsLimit / 2;
        }

        private static Population InitializePopulation(Environment env, IChromosome originChromosome, IFitnessFunction fitness, Solutions previousSolutions)
        {
            IChromosome chromosome;
            if (null == originChromosome)
                chromosome = new MyChromosome(env);
            else
                chromosome = originChromosome.Clone();

            Population population = new Population(env.PopulationSize, chromosome, fitness, env.Selection);
            if (env.HeadStartIndividualsOnPopulationInit)
            {
                if (originChromosome != null)
                    population.AddChromosome(((MyChromosome)originChromosome.Clone()).NegateValues());
                for (int i = 0; i < previousSolutions.SolutionsData.Count; i++)
                    population.AddChromosome(new MyChromosome(env, previousSolutions.SolutionsData[i].Solution.InputValuesArray));
            }
            population.CrossoverRate = env.CrossoverRate;
            population.MutationRate = env.MutationRate;
            population.RandomSelectionPortion = env.RandomSelectionPortion;
            return population;
        }
    }
}
