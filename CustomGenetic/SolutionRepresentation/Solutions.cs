﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;
using DecisionsModel;
using DecisionsModel.TestTargetsGenerators;

namespace CustomGenetic.SolutionRepresentation
{
    class Solutions : EqualityCompartor, IComparable<Solutions>
    {
        public Environment Env { get; private set; }

        public List<SingleTargetSolutionData> SolutionsData { get; private set; }
        public List<SingleTargetSolutionData> OptimizedSolutionsData { get; private set; }

        public int NumberOfAchievedBVCTargetsFromMCDCSolutions { get; set; }

        public int OptimizedTestSuiteSize
        {
            get
            {
                if (null == OptimizedSolutionsData)
                    FillOptimizedSolutionsData();
                return OptimizedSolutionsData.Count;
            }
        }

        public int NumberOfTestTargets { get; set; }
        public int NumberOfReachedTestTargets { get; set; }

        public double SatisfactionPercentage
        {
            get { return CalculateOverallCoverage(); }
        }

        public long GenerationsConsumed { get; set; }
        public long EffectiveGenerationsConsumed { get; set; }

        public double EffectiveGenerationsPercentage
        {
            get { return EffectiveGenerationsConsumed * 100.0 / GenerationsConsumed; }
        }

        public PossibleMCDCCombination MCDCCombination { get; private set; }

        public Solutions(Environment env, PossibleMCDCCombination mcdcCombination)
        {
            Env = env;
            MCDCCombination = mcdcCombination;
            SolutionsData = new List<SingleTargetSolutionData>();
        }

        public int CompareTo(Solutions other)
        {
            if (null == other)
                return -1;
            if (AreEqual(SatisfactionPercentage, other.SatisfactionPercentage))
            {
                if (AreEqual(EffectiveGenerationsPercentage, other.EffectiveGenerationsPercentage))
                    return 0;
                else if (EffectiveGenerationsPercentage > other.EffectiveGenerationsPercentage)
                    return -1;
                else
                    return 1;
            }
            else if (SatisfactionPercentage > other.SatisfactionPercentage)
                return -1;
            else
                return 1;
        }

        public void UpdateWithTheseSolutions(Solutions other, Decision decision)
        {
            if (Env != other.Env)
                throw new Exception("CurrentRunSettings should match!");
            SolutionsData.AddRange(other.SolutionsData);
            NumberOfTestTargets += other.NumberOfTestTargets;
            NumberOfReachedTestTargets += other.NumberOfReachedTestTargets;
            GenerationsConsumed += other.GenerationsConsumed;
            EffectiveGenerationsConsumed += other.EffectiveGenerationsConsumed;
            decision.ChoosenMCDCCombination = other.MCDCCombination;
        }

        public double CalculateOverallCoverage()
        {
            int achievedTestTargets = ExecuteTestDataAndGetAchievedTestTargetsCount(false);
            return achievedTestTargets * 100.0 / NumberOfTestTargets;
        }

        public SingleTargetSolutionData GetTestDataThatExecuteThisTestTarget(Decision decision, TruthTableRow testCase)
        {
            for (int i = 0; i < OptimizedSolutionsData.Count; i++)
            {
                Env.RunProgram(OptimizedSolutionsData[i].SolutionData);
                if (IsTheDesiredTestCaseExecuted(decision, testCase))
                    return OptimizedSolutionsData[i];
            }
            return null;
        }

        private bool IsTheDesiredTestCaseExecuted(Decision decision, TruthTableRow testCase)
        {
            if (null != decision.EvaluatedTruthTableRow)
            {
                if (Env.UseDontCaresToMinimize)
                {
                    if (testCase.IsTruthTableRowEquivalentConsideringDontCares(decision.EvaluatedTruthTableRow, Env.ConsiderDontCaresAndOutputValues))
                        return true;
                }
                else
                    if (testCase.Equals(decision.EvaluatedTruthTableRow))
                        return true;
            }
            return false;
        }

        private int ExecuteTestDataAndGetAchievedTestTargetsCount(bool checkTheEvaluatedRowEvenIfRowIsPreviouslySatisfied)
        {
            foreach (SingleTargetSolutionData solution in SolutionsData)
            {
                solution.MainTestTarget.SatisfiedBefore = false;
                solution.NumberOfSatisfiedTargets = 0;
                foreach (TestTarget testTarget in solution.ExtraSatisfiedTestTargets)
                    testTarget.SatisfiedBefore = false;
            }

            int totalAchievedTestTargets = 0;

            for (int i = 0; i < SolutionsData.Count; i++)
            {
                if (null != SolutionsData[i].MainTestTarget.Condition)
                    SolutionsData[i].MainTestTarget.Decision.ConditionsList[SolutionsData[i].MainTestTarget.Condition.OriginBVCTarget.ConditionIndex].Alter(SolutionsData[i].MainTestTarget.Condition);
                else
                    Env.RunProgram(SolutionsData[i].SolutionData);
                for (int j = 0; j < SolutionsData.Count; j++)
                {
                    if (null != SolutionsData[i].MainTestTarget.Condition && null == SolutionsData[j].MainTestTarget.Condition || null == SolutionsData[i].MainTestTarget.Condition && null != SolutionsData[j].MainTestTarget.Condition)
                        throw new Exception("Solutions from different coverage criteria. They should not co-exist in the same solutions set");

                    if (null != SolutionsData[j].MainTestTarget.Condition)
                    {
                        SolutionsData[j].MainTestTarget.Decision.ConditionsList[SolutionsData[j].MainTestTarget.Condition.OriginBVCTarget.ConditionIndex].Alter(SolutionsData[j].MainTestTarget.Condition);
                        Env.RunProgram(SolutionsData[i].SolutionData);
                    }

                    if (SolutionsData[j].MainTestTarget.SatisfiedBefore && !checkTheEvaluatedRowEvenIfRowIsPreviouslySatisfied)
                        continue;
                    SolutionsData[j].MainTestTarget.SatisfiedBefore = IsTheDesiredTestCaseExecuted(SolutionsData[j].MainTestTarget.Decision, SolutionsData[j].MainTestTarget.TestCase);
                    if (SolutionsData[j].MainTestTarget.SatisfiedBefore)
                    {
                        SolutionsData[i].NumberOfSatisfiedTargets++;
                        if (i != j)
                            SolutionsData[i].ExtraSatisfiedTestTargets.Add(SolutionsData[j].MainTestTarget);
                        totalAchievedTestTargets++;
                    }
                    if (null != SolutionsData[j].MainTestTarget.Condition)
                        SolutionsData[j].MainTestTarget.Decision.ConditionsList[SolutionsData[j].MainTestTarget.Condition.OriginBVCTarget.ConditionIndex].RestoreOriginalCondition();
                }
                if (null != SolutionsData[i].MainTestTarget.Condition)
                    SolutionsData[i].MainTestTarget.Decision.ConditionsList[SolutionsData[i].MainTestTarget.Condition.OriginBVCTarget.ConditionIndex].RestoreOriginalCondition();
            }

            return totalAchievedTestTargets;
        }

        private void FillOptimizedSolutionsData()
        {
            if (null == OptimizedSolutionsData)
                OptimizedSolutionsData = new List<SingleTargetSolutionData>();
            ExecuteTestDataAndGetAchievedTestTargetsCount(true);
            SolutionsData.Sort();
            ExecuteTestDataAndGetAchievedTestTargetsCount(false);

            for (int i = 0; i < SolutionsData.Count; i++)
                if (SolutionsData[i].NumberOfSatisfiedTargets > 0)
                    OptimizedSolutionsData.Add(SolutionsData[i]);
        }
    }
}
