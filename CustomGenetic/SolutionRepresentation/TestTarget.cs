﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using DecisionsModel;
using DecisionsModel.TestTargetsGenerators;

namespace CustomGenetic.SolutionRepresentation
{
    class TestTarget
    {
        public Decision Decision { get; private set; }
        public ConditionPossibleAlternation Condition { get; private set; }
        public TruthTableRow TestCase { get; private set; }
        public PossibleMCDCCombination MCDCCombination { get; private set; }
        public bool SatisfiedBefore { get; set; }

        public TestTarget(Decision decision, TruthTableRow testCase, PossibleMCDCCombination mcdcCombination, ConditionPossibleAlternation condition)
        {
            Decision = decision;
            Condition = condition;
            TestCase = testCase;
            MCDCCombination = mcdcCombination;
        }
    }
}
