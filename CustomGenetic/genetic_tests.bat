set numberOfRunsPerInstance=30
set inputRange=double
time /t
REM del .\*.csv

.\CustomGenetic .\output %numberOfRunsPerInstance% NextDateWithIfDouble %inputRange%
time /t

.\CustomGenetic .\output %numberOfRunsPerInstance% TriangleClassificationWithoutData %inputRange%
time /t

.\CustomGenetic .\output %numberOfRunsPerInstance% NextDateWithIfInt %inputRange%
time /t

echo off
REM .\CustomGenetic .\output %numberOfRunsPerInstance% EqualityGenerationsMeasurement %inputRange%
REM time /t

REM .\CustomGenetic .\output %numberOfRunsPerInstance% InEqualityGenerationsMeasurement %inputRange%
REM time /t

REM .\CustomGenetic .\output %numberOfRunsPerInstance% NextDateWithSwitch %inputRange%
REM time /t

REM .\CustomGenetic .\output %numberOfRunsPerInstance% TriangleClassificationWithData %inputRange%
REM time /t
echo on

pause