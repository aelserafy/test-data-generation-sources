﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;
using DecisionsModel.TestTargetsGenerators;

namespace DecisionsModel
{
    public enum RelationalOperator { EqualEqual, NotEqual, LessThan, LessThanOrEqual, MoreThan, MoreThanOrEqual };
    public delegate double EvaluateConditionSideDelegate(double[] inputs);

    public class Condition : DecisionElement
    {
        public static Condition GetDummyCondition(int id)
        {
            return new Condition(id, null, RelationalOperator.EqualEqual, null);
        }

        public int ID { get; private set; }
        public string Text { get; set; }
        public RelationalOperator RelationalOperator { get; private set; }
        public EvaluateConditionSideDelegate LeftSide { get; private set; }
        public EvaluateConditionSideDelegate RightSide { get; private set; }
        public ConditionPossibleAlternation OriginalCondition { get; private set; }
        public Decision RootDecision { get; private set; }
        public bool ConditionBooleanValue { get; private set; }
        public bool ConditionDontCareValue { get; private set; }

        public Condition(int id, EvaluateConditionSideDelegate leftSide, RelationalOperator relationalOperator, EvaluateConditionSideDelegate rightSide)
        {
            ID = id;
            LeftSide = leftSide;
            RelationalOperator = relationalOperator;
            RightSide = rightSide;
            OriginalCondition = new ConditionPossibleAlternation(null, RightSide, RelationalOperator, 0);
        }
        public Condition(bool value, bool dontCare)
        {
            ConditionBooleanValue = value;
            ConditionDontCareValue = dontCare;
        }

        public List<TruthTableRow> GetTTRowsThatCausesThisConditionToEvaluate(bool useDontCaresToMinimize)
        {
            return RootDecision.GetTTRowsThatCausesThisConditionToEvaluate(this, useDontCaresToMinimize);
        }

        public override TruthTableRow Evaluate(double[] inputs, int maxNumberOfMCDCTestAlternativesToGenerate, bool useDontCaresToMinimize)
        {
            switch (RelationalOperator)
            {
                case RelationalOperator.EqualEqual:
                    ConditionBooleanValue = AreEqual(LeftSide(inputs), RightSide(inputs)); break;
                case RelationalOperator.NotEqual:
                    ConditionBooleanValue = !AreEqual(LeftSide(inputs), RightSide(inputs)); break;
                case RelationalOperator.LessThan:
                    ConditionBooleanValue = LeftSide(inputs) < RightSide(inputs); break;
                case RelationalOperator.LessThanOrEqual:
                    ConditionBooleanValue = LeftSide(inputs) <= RightSide(inputs) || AreEqual(LeftSide(inputs), RightSide(inputs)); break;
                case RelationalOperator.MoreThan:
                    ConditionBooleanValue = LeftSide(inputs) > RightSide(inputs); break;
                case RelationalOperator.MoreThanOrEqual:
                    ConditionBooleanValue = LeftSide(inputs) >= RightSide(inputs) || AreEqual(LeftSide(inputs), RightSide(inputs)); break;
                default:
                    throw new Exception("Unhandled relational operator in condition");
            }

            TruthTableRow ttRow = new TruthTableRow(1, ConditionBooleanValue ? 1 : 0, 0, ConditionBooleanValue);
            return ttRow;
        }

        public override string GetReconstructedText()
        {
            return Text;
        }
        public override string GetSimplifiedText()
        {
            return "Condition_" + ID;
        }

        public override string GetTabbedTextWithNewLines(int numberOfTabs)
        {
            string tabs = "";
            for (int i = 0; i < numberOfTabs; i++)
                tabs += "\t";
            return tabs + GetSimplifiedText() + ": " + GetReconstructedText();
        }

        public override List<string> GetSimplifiedConditionsList()
        {
            return new List<string>() { GetSimplifiedText() };
        }

        public override TruthTableRow[] GenerateTruthTable()
        {
            TruthTableRow[] truthTable = new TruthTableRow[2];

            truthTable[0] = new TruthTableRow(1, 0, 0, false);
            truthTable[1] = new TruthTableRow(1, 1, 0, true);

            return truthTable;
        }

        public override double CalcBranchDistance(bool desiredPath, TruthTableRow desiredRow, double[] inputs, double branchDistanceConstant, bool useMessedUpBDFitness)
        {
            double conditionBranchDistance;

            desiredPath = desiredRow.GetConditionValue(ID);

            switch (RelationalOperator)
            {
                case RelationalOperator.EqualEqual:
                    conditionBranchDistance = EqualEqualBranchDistanceCalc(LeftSide(inputs), RightSide(inputs), desiredPath, branchDistanceConstant); break;
                case RelationalOperator.NotEqual:
                    conditionBranchDistance = NotEqualBranchDistanceCalc(LeftSide(inputs), RightSide(inputs), desiredPath, branchDistanceConstant); break;
                case RelationalOperator.LessThan:
                    conditionBranchDistance = LessThanBranchDistanceCalc(LeftSide(inputs), RightSide(inputs), desiredPath, branchDistanceConstant, useMessedUpBDFitness); break;
                case RelationalOperator.LessThanOrEqual:
                    conditionBranchDistance = LessThanOrEqualBranchDistanceCalc(LeftSide(inputs), RightSide(inputs), desiredPath, branchDistanceConstant, useMessedUpBDFitness); break;
                case RelationalOperator.MoreThan:
                    conditionBranchDistance = MoreThanBranchDistanceCalc(LeftSide(inputs), RightSide(inputs), desiredPath, branchDistanceConstant, useMessedUpBDFitness); break;
                case RelationalOperator.MoreThanOrEqual:
                    conditionBranchDistance = MoreThanOrEqualBranchDistanceCalc(LeftSide(inputs), RightSide(inputs), desiredPath, branchDistanceConstant, useMessedUpBDFitness); break;
                default:
                    throw new Exception("Unhandled relational operator in condition");
            }

            return conditionBranchDistance;
        }

        private double EqualEqualBranchDistanceCalc(double a, double b, bool path, double k)
        {
            if (path)
                return Math.Abs(a - b);
            else
                return AreEqual(a, b) ? k : 0;
        }

        private double NotEqualBranchDistanceCalc(double a, double b, bool path, double k)
        {
            if (path)
                return !AreEqual(a, b) ? 0 : k;
            else
                return !AreEqual(a, b) ? Math.Abs(a - b) : 0;
        }

        private double LessThanBranchDistanceCalc(double a, double b, bool path, double k, bool useMessedUpBDFitness)
        {
            if (path)
                return a < b ? 0 : Math.Abs(a - b) + k;
            else
                return a < b ? useMessedUpBDFitness ? Math.Abs(a - b) + k : Math.Abs(a - b) : 0;
        }

        private double LessThanOrEqualBranchDistanceCalc(double a, double b, bool path, double k, bool useMessedUpBDFitness)
        {
            if (path)
                return a <= b || AreEqual(a, b) ? 0 : (Math.Abs(a - b));
            else
                return a <= b || AreEqual(a, b) ? useMessedUpBDFitness ? Math.Abs(a - b) : Math.Abs(a - b) + k : 0;
        }

        private double MoreThanBranchDistanceCalc(double a, double b, bool path, double k, bool useMessedUpBDFitness)
        {
            if (path)
                return a > b ? 0 : Math.Abs(a - b) + k;
            else
                return a > b ? useMessedUpBDFitness ? Math.Abs(a - b) + k : Math.Abs(a - b) : 0;
        }

        private double MoreThanOrEqualBranchDistanceCalc(double a, double b, bool path, double k, bool useMessedUpBDFitness)
        {
            if (path)
                return a >= b || AreEqual(a, b) ? 0 : (Math.Abs(a - b));
            else
                return a >= b || AreEqual(a, b) ? useMessedUpBDFitness ? Math.Abs(a - b) : Math.Abs(a - b) + k : 0;
        }

        public override void SetRootDecision(Decision rootDecision)
        {
            RootDecision = rootDecision;
            rootDecision.ConditionsList.Add(this);
        }

        public override void GetEqualEqualAndNonEqualEqualPredicatesCount(ref int equalEqualPredicatesCount, ref int nonEqualEqualPredicatesCount)
        {
            if (RelationalOperator.EqualEqual == RelationalOperator || RelationalOperator.NotEqual == RelationalOperator)
                equalEqualPredicatesCount++;
            else
                nonEqualEqualPredicatesCount++;
        }

        public void Alter(ConditionPossibleAlternation conditionPossibleAlternation)
        {
            if (null == conditionPossibleAlternation || null == conditionPossibleAlternation.RightSide)
                throw new Exception("Undefined alternation of a condition");

            RightSide = conditionPossibleAlternation.RightSide;
            RelationalOperator = conditionPossibleAlternation.RelationalOperator;
        }

        public void RestoreOriginalCondition()
        {
            Alter(OriginalCondition);
        }
    }
}
