﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;
using DecisionsModel.TestTargetsGenerators;

namespace DecisionsModel
{
    public enum LogicalOperatorType { Not, And, Or };

    public class Decision : DecisionElement
    {
        public int ID { get; set; }
        public DecisionElement FirstOperand { get; private set; }
        public LogicalOperatorType? LogicalOperator { get; private set; }
        public DecisionElement SecondOperand { get; private set; }
        public List<PossibleMCDCCombination> MCDCAlternatives { get; private set; }
        public List<ConditionBVCTarget> BVCTestTargets { get; private set; }
        public TruthTableRow[] DecisionTruthTable { get; private set; }
        public TruthTableRow EvaluatedTruthTableRow { get; set; }
        public PossibleMCDCCombination ChoosenMCDCCombination { get; set; }
        public List<Dictionary<Decision, bool>> ParallelPathsOfDependentsAndExpectedOutcomes { get; set; }
        public List<Condition> ConditionsList { get; private set; }

        public Decision()
        {
            ParallelPathsOfDependentsAndExpectedOutcomes = new List<Dictionary<Decision, bool>>();
            ConditionsList = new List<Condition>();
        }
        public Decision(DecisionElement firstOperand, LogicalOperatorType? logicalOperator = null)
            : this()
        {
            FirstOperand = firstOperand;
            LogicalOperator = logicalOperator;
            FirstOperand.SetRootDecision(this);
        }
        public Decision(DecisionElement firstOperand, DecisionElement secondOperand, LogicalOperatorType? logicalOperator = null)
            : this(firstOperand, logicalOperator)
        {
            SecondOperand = secondOperand;
            SecondOperand.SetRootDecision(this);
        }

        public List<TruthTableRow> GetTTRowsThatCausesThisConditionToEvaluate(Condition condition, bool useDontCaresToMinimize)
        {
            if (this != condition.RootDecision)
                throw new Exception("This condition doesn't belong to this decision");
            if (null == DecisionTruthTable)
                GenerateTruthTable();
            List<TruthTableRow> ttRowsToBeReturned = new List<TruthTableRow>();
            int i;
            for (i = 0; i < DecisionTruthTable.Length; i++)
                if (DecisionTruthTable[i].GetConditionValue(condition.ID) && !DecisionTruthTable[i].GetDontCareMaskValue(condition.ID))
                    ttRowsToBeReturned.Add(DecisionTruthTable[i]);
            if (useDontCaresToMinimize)
            {
                for (i = 0; i < ttRowsToBeReturned.Count; i++)
                {
                    int newDontCaresMask = 0;
                    for (int k = 0; k < DecisionTruthTable[0].NumberOfConditions - condition.ID - 1; k++)
                        newDontCaresMask |= (1 << k);
                    ttRowsToBeReturned[i] = new TruthTableRow(ttRowsToBeReturned[i].NumberOfConditions, ttRowsToBeReturned[i].RowIndex, ttRowsToBeReturned[i].DontCaresMask | newDontCaresMask, ttRowsToBeReturned[i].Output);
                }
                ttRowsToBeReturned.Sort(new TruthTableRowComparatorFavouringDontCares());
                i = 0;
                while (i < ttRowsToBeReturned.Count)
                {
                    int j = i + 1;
                    while (j < ttRowsToBeReturned.Count)
                    {
                        bool removedMatch = false;
                        if (ttRowsToBeReturned[i].IsTruthTableRowEquivalentConsideringDontCares(ttRowsToBeReturned[j], false))
                        {
                            ttRowsToBeReturned.RemoveAt(j);
                            removedMatch = true;
                        }
                        if (!removedMatch)
                            j++;
                    }
                    i++;
                }
            }
            return ttRowsToBeReturned;
        }

        public override List<string> GetSimplifiedConditionsList()
        {
            List<string> conditionsList = new List<string>();
            conditionsList.AddRange(FirstOperand.GetSimplifiedConditionsList());
            if (null != SecondOperand)
                conditionsList.AddRange(SecondOperand.GetSimplifiedConditionsList());
            return conditionsList;
        }

        public List<PossibleMCDCCombination> GenerateMCDCTestAlternatives(int maxNumberOfMCDCTestAlternativesToGenerate, bool useDontCaresToMinimize)
        {
            if (null == MCDCAlternatives)
            {
                MCDCTestAlternativesGenerator mcdcGenerator = new MCDCTestAlternativesGenerator(maxNumberOfMCDCTestAlternativesToGenerate, useDontCaresToMinimize);
                MCDCAlternatives = mcdcGenerator.GenerateAllPossibleMCDCTestAlternatives(this);
            }
            return MCDCAlternatives;
        }

        public List<ConditionBVCTarget> GenerateBVCTestTargets(double leastIncrementalValue, bool useDontCaresToMinimize)
        {
            if (null == BVCTestTargets)
            {
                BVCTargetsGenerator bvcGenerator = new BVCTargetsGenerator(leastIncrementalValue, useDontCaresToMinimize);
                BVCTestTargets = bvcGenerator.GenerateBVCTestTargets(this);
            }
            return BVCTestTargets;
        }

        public override TruthTableRow[] GenerateTruthTable()
        {
            TruthTableRow[] firstOperandTT = FirstOperand.GenerateTruthTable();
            TruthTableRow[] secondOperandTT = null;
            if (null != SecondOperand)
                secondOperandTT = SecondOperand.GenerateTruthTable();

            DecisionTruthTable = CombineTruthTables(firstOperandTT, secondOperandTT);
            return DecisionTruthTable;
        }

        public TruthTableRow GetTruthTableRow(int encodedRowIndex)
        {
            for (int i = 0; i < DecisionTruthTable.Length; i++)
                if (DecisionTruthTable[i].RowIndex == encodedRowIndex)
                    return DecisionTruthTable[i];
            return null;
        }

        public bool? GetTruthTableRowOutput(int encodedRowIndex)
        {
            for (int i = 0; i < DecisionTruthTable.Length; i++)
                if (DecisionTruthTable[i].RowIndex == encodedRowIndex)
                    return DecisionTruthTable[i].Output;
            return null;
        }

        public override TruthTableRow Evaluate(double[] inputs, int maxNumberOfMCDCTestAlternativesToGenerate, bool useDontCaresToMinimize)
        {
            if (null == DecisionTruthTable)
                GenerateTruthTable();
            if (null == MCDCAlternatives)
                GenerateMCDCTestAlternatives(maxNumberOfMCDCTestAlternativesToGenerate, useDontCaresToMinimize);

            TruthTableRow toBeReturnTTRow;

            TruthTableRow firstOperandTT = FirstOperand.Evaluate(inputs, maxNumberOfMCDCTestAlternativesToGenerate, useDontCaresToMinimize);
            if (null == SecondOperand)
            {
                if (LogicalOperatorType.Not == LogicalOperator)
                    firstOperandTT.InvertOutput();

                toBeReturnTTRow = firstOperandTT;
            }
            else
            {
                TruthTableRow secondOperandTT = SecondOperand.Evaluate(inputs, maxNumberOfMCDCTestAlternativesToGenerate, useDontCaresToMinimize);
                toBeReturnTTRow = CombineTruthTableRows(firstOperandTT, secondOperandTT);
            }

            EvaluatedTruthTableRow = toBeReturnTTRow;

            return toBeReturnTTRow;
        }

        private TruthTableRow[] CombineTruthTables(TruthTableRow[] first, TruthTableRow[] second)
        {
            if (null == second)
            {
                if (LogicalOperatorType.Not == LogicalOperator)
                    for (int i = 0; i < first.Length; i++)
                        first[i].InvertOutput();
                return first;
            }

            int rowsCount = first.Length * second.Length;
            TruthTableRow[] combinedTT = new TruthTableRow[rowsCount];

            for (int i = 0; i < first.Length; i++)
                for (int j = 0; j < second.Length; j++)
                    combinedTT[i * second.Length + j] = CombineTruthTableRows(first[i], second[j]);
            return combinedTT;
        }

        private TruthTableRow CombineTruthTableRows(TruthTableRow firstTTRow, TruthTableRow secondTTRow)
        {
            int firstNumberOfConditions = firstTTRow.NumberOfConditions;
            int secondNumberOfConditions = secondTTRow.NumberOfConditions;
            int dontCaresMask = secondTTRow.DontCaresMask;

            if ((firstTTRow.Output && LogicalOperator == LogicalOperatorType.Or) || (!firstTTRow.Output && LogicalOperator == LogicalOperatorType.And))
                for (int k = 0; k < secondNumberOfConditions; k++)
                    dontCaresMask = dontCaresMask | (1 << k);
            bool combinedOutput = GetCombinedRowsOutput(firstTTRow, secondTTRow);
            TruthTableRow newRow = new TruthTableRow((byte)(firstNumberOfConditions + secondNumberOfConditions), (firstTTRow.RowIndex << secondNumberOfConditions) | secondTTRow.RowIndex, (firstTTRow.DontCaresMask << secondNumberOfConditions) | dontCaresMask, combinedOutput);
            return newRow;
        }

        private bool GetCombinedRowsOutput(TruthTableRow firstOperandTT, TruthTableRow secondOperandTT)
        {
            bool combinedOutput;
            if (LogicalOperatorType.And == LogicalOperator)
                combinedOutput = firstOperandTT.Output && secondOperandTT.Output;
            else if (LogicalOperatorType.Or == LogicalOperator)
                combinedOutput = firstOperandTT.Output || secondOperandTT.Output;
            else
                throw new Exception("Invalid decision logical operator");
            return combinedOutput;
        }

        private string GetOperatorText()
        {
            if (LogicalOperatorType.And == LogicalOperator)
                return " && ";
            else if (LogicalOperatorType.Or == LogicalOperator)
                return " || ";
            else if (LogicalOperatorType.Not == LogicalOperator)
                return "!";
            return "";
        }

        public override string GetReconstructedText()
        {
            string operatorText = GetOperatorText();
            if (LogicalOperatorType.Not == LogicalOperator)
            {
                string firstOperandText = FirstOperand.GetReconstructedText();
                int parenthesesBalance = 0;
                bool appendParentheses = true;
                for (int i = 0; appendParentheses && i < firstOperandText.Length; i++)
                {
                    if ('(' == firstOperandText[i])
                        parenthesesBalance++;
                    else if (')' == firstOperandText[i])
                        parenthesesBalance--;
                    if (0 == parenthesesBalance)
                        appendParentheses = false;
                }
                if (appendParentheses)
                    return operatorText + "(" + firstOperandText + ")";
                return operatorText + firstOperandText;
            }

            if (null == SecondOperand && null == LogicalOperator)
                return FirstOperand.GetReconstructedText();

            return "(" + FirstOperand.GetReconstructedText() + operatorText + SecondOperand.GetReconstructedText() + ")";
        }

        public override string GetSimplifiedText()
        {
            string operatorText = GetOperatorText();
            if (LogicalOperatorType.Not == LogicalOperator)
                return operatorText + FirstOperand.GetReconstructedText();

            if (null == SecondOperand && null == LogicalOperator)
                return FirstOperand.GetSimplifiedText();

            return "(" + FirstOperand.GetSimplifiedText() + operatorText + SecondOperand.GetSimplifiedText() + ")";
        }

        public override string GetTabbedTextWithNewLines(int numberOfTabs)
        {
            string tabs = "";
            for (int i = 0; i < numberOfTabs; i++)
                tabs += "\t";

            string operatorText = "";
            if (LogicalOperatorType.And == LogicalOperator)
                operatorText = "&&";
            else if (LogicalOperatorType.Or == LogicalOperator)
                operatorText = "||";
            else if (LogicalOperatorType.Not == LogicalOperator)
                return tabs + "\t!" + Environment.NewLine + FirstOperand.GetTabbedTextWithNewLines(numberOfTabs + 1);

            if (null == SecondOperand && null == LogicalOperator)
                return FirstOperand.GetTabbedTextWithNewLines(numberOfTabs);

            return tabs + "(" + Environment.NewLine
                                + FirstOperand.GetTabbedTextWithNewLines(numberOfTabs + 1) + Environment.NewLine
                                + tabs + "\t" + operatorText + Environment.NewLine
                                + SecondOperand.GetTabbedTextWithNewLines(numberOfTabs + 1) + Environment.NewLine
                    + tabs + ")";
        }

        public override double CalcBranchDistance(bool desiredPath, TruthTableRow desiredRow, double[] inputs, double branchDistanceConstant, bool useMessedUpBDFitness)
        {
            double branchDistanceToBeReturned;
            if (null == desiredRow)
            {
                //First possible implementation
                //for (int i = 0; i < DecisionTruthTable.Outputs.Length; i++)
                //{
                //    if (DecisionTruthTable.Outputs[i] == desiredPath)
                //    {
                //        double tempBranchDistance = CalcBranchDistanceForACertainTTRow(desiredPath, DecisionTruthTable.Values[i], inputs, branchDistanceConstant, false);
                //        if (tempBranchDistance < branchDistanceToBeReturned)
                //            branchDistanceToBeReturned = tempBranchDistance;
                //    }
                //}

                //Second possible implementation
                TruthTableRow ttRowToTarget;
                if (desiredPath)
                    ttRowToTarget = DecisionTruthTable[DecisionTruthTable.Length - 1];
                else
                    ttRowToTarget = DecisionTruthTable[0];
                branchDistanceToBeReturned = CalcBranchDistanceForACertainTTRow(desiredPath, ttRowToTarget, inputs, branchDistanceConstant, true, useMessedUpBDFitness);
            }
            else
                branchDistanceToBeReturned = CalcBranchDistanceForACertainTTRow(desiredPath, desiredRow, inputs, branchDistanceConstant, false, useMessedUpBDFitness);

            return branchDistanceToBeReturned;
        }

        private double CalcBranchDistanceForACertainTTRow(bool desiredPath, TruthTableRow desiredRow, double[] inputs, double branchDistanceConstant, bool isDivergentNode, bool useMessedUpBDFitness)
        {
            double firstOperandBranchDistance = FirstOperand.CalcBranchDistance(desiredPath, desiredRow, inputs, branchDistanceConstant, useMessedUpBDFitness);

            if (null != SecondOperand)
            {
                double secondOperandBranchDistance = SecondOperand.CalcBranchDistance(desiredPath, desiredRow, inputs, branchDistanceConstant, useMessedUpBDFitness);
                if (!isDivergentNode || (LogicalOperatorType.And == LogicalOperator && desiredPath) || (LogicalOperatorType.Or == LogicalOperator && !desiredPath))
                    return firstOperandBranchDistance + secondOperandBranchDistance;
                else
                    return Math.Min(firstOperandBranchDistance, secondOperandBranchDistance);
            }
            else
                return firstOperandBranchDistance;
        }

        public override void SetRootDecision(Decision rootDecision)
        {
            FirstOperand.SetRootDecision(rootDecision);
            if (null != SecondOperand)
                SecondOperand.SetRootDecision(rootDecision);
        }

        public override void GetEqualEqualAndNonEqualEqualPredicatesCount(ref int equalEqualPredicatesCount, ref int nonEqualEqualPredicatesCount)
        {
            FirstOperand.GetEqualEqualAndNonEqualEqualPredicatesCount(ref equalEqualPredicatesCount, ref nonEqualEqualPredicatesCount);
            if (null != SecondOperand)
                SecondOperand.GetEqualEqualAndNonEqualEqualPredicatesCount(ref equalEqualPredicatesCount, ref nonEqualEqualPredicatesCount);
        }
    }
}
