﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;

namespace DecisionsModel
{
    public abstract class DecisionElement : EqualityCompartor
    {
        public abstract string GetReconstructedText();
        public abstract string GetSimplifiedText();
        public abstract string GetTabbedTextWithNewLines(int numberOfTabs);
        public abstract List<string> GetSimplifiedConditionsList();
        public abstract TruthTableRow[] GenerateTruthTable();
        public abstract TruthTableRow Evaluate(double[] inputs, int maxNumberOfMCDCTestAlternativesToGenerate, bool useDontCaresToMinimize);
        public Dictionary<string, List<int>> ConditionsMap;
        public abstract double CalcBranchDistance(bool desiredPath, TruthTableRow desiredRow, double[] inputs, double branchDistanceConstant, bool useMessedUpBDFitness);
        public abstract void SetRootDecision(Decision rootDecision);
        public abstract void GetEqualEqualAndNonEqualEqualPredicatesCount(ref int equalEqualPredicatesCount, ref int nonEqualEqualPredicatesCount);
    }
}
