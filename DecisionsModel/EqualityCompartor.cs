﻿using System;
namespace DecisionsModel
{
    public abstract class EqualityCompartor
    {
        public bool AreEqual(double x, double y)
        {
            return Math.Abs(x - y) < 0.00000001;
        }

        public static bool StaticAreEqual(double x, double y)
        {
            ConcreteEqualityComparator comparator = new ConcreteEqualityComparator();
            return comparator.AreEqual(x, y);
        }

        private class  ConcreteEqualityComparator:EqualityCompartor
        {
             
        }
}
}
