﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;

namespace DecisionsModel.TestTargetsGenerators
{
    public class BVCTargetsGenerator
    {
        private double LeastIncrementalValue { get; set; }
        private bool UseDontCaresToMinimize { get; set; }

        public BVCTargetsGenerator(double leastIncrementalValue, bool useDontCaresToMinimize)
        {
            LeastIncrementalValue = leastIncrementalValue;
            UseDontCaresToMinimize = useDontCaresToMinimize;
        }

        public List<ConditionBVCTarget> GenerateBVCTestTargets(Decision decision)
        {
            List<ConditionBVCTarget> bvcTestTargets = new List<ConditionBVCTarget>();
            for (int i = 0; i < decision.ConditionsList.Count; i++)
            {
                int conditionIndex = i; // this is a workaround for the lambda expressions' lazy evaluation!!
                ConditionBVCTarget bvcTestTarget = new ConditionBVCTarget(decision, i);
                bvcTestTargets.Add(bvcTestTarget);
                bvcTestTarget.TruthTableRowsThatCauseThisConditionToEvaluate = decision.ConditionsList[conditionIndex].GetTTRowsThatCausesThisConditionToEvaluate(UseDontCaresToMinimize);

                bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, decision.ConditionsList[conditionIndex].OriginalCondition.RightSide, RelationalOperator.EqualEqual, 1));
                switch (decision.ConditionsList[i].RelationalOperator)
                {
                    case RelationalOperator.EqualEqual:
                    case RelationalOperator.NotEqual:
                        {
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, inputs => decision.ConditionsList[conditionIndex].OriginalCondition.RightSide(inputs) + LeastIncrementalValue, RelationalOperator.EqualEqual, 2));
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, inputs => decision.ConditionsList[conditionIndex].OriginalCondition.RightSide(inputs) - LeastIncrementalValue, RelationalOperator.EqualEqual, 3));
                            break;
                        }
                    case RelationalOperator.LessThan:
                    case RelationalOperator.MoreThanOrEqual:
                        {
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, decision.ConditionsList[conditionIndex].OriginalCondition.RightSide, RelationalOperator.MoreThan, 2));
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, inputs => decision.ConditionsList[conditionIndex].OriginalCondition.RightSide(inputs) - LeastIncrementalValue, RelationalOperator.EqualEqual, 3));
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, inputs => decision.ConditionsList[conditionIndex].OriginalCondition.RightSide(inputs) - LeastIncrementalValue, RelationalOperator.LessThan, 4));
                            break;
                        }
                    case RelationalOperator.LessThanOrEqual:
                    case RelationalOperator.MoreThan:
                        {
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, decision.ConditionsList[conditionIndex].OriginalCondition.RightSide, RelationalOperator.LessThan, 2));
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, inputs => decision.ConditionsList[conditionIndex].OriginalCondition.RightSide(inputs) + LeastIncrementalValue, RelationalOperator.EqualEqual, 3));
                            bvcTestTarget.ReplacingConditions.Add(new ConditionPossibleAlternation(bvcTestTarget, inputs => decision.ConditionsList[conditionIndex].OriginalCondition.RightSide(inputs) + LeastIncrementalValue, RelationalOperator.MoreThan, 4));
                            break;
                        }
                    default:
                        throw new Exception("Something went wrong, this shouldn't happen!!");
                }
            }
            return bvcTestTargets;
        }
    }
}
