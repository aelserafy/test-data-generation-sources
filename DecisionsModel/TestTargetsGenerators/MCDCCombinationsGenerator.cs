﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using AForge;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DecisionsModel.TestTargetsGenerators
{
    public class MCDCTestAlternativesGenerator
    {
        private int MaxNumberOfMCDCTestAlternativesToGenerate { get; set; }
        private bool UseDontCaresToMinimize { get; set; }
        public int NumberOfSavedMCDCTargetsFromDontCares { get; private set; }

        public MCDCTestAlternativesGenerator(int maxNumberOfMCDCTestAlternativesToGenerate, bool useDontCaresToMinimize)
        {
            MaxNumberOfMCDCTestAlternativesToGenerate = maxNumberOfMCDCTestAlternativesToGenerate;
            UseDontCaresToMinimize = useDontCaresToMinimize;
        }

        public List<PossibleMCDCCombination> GenerateAllPossibleMCDCTestAlternatives(Decision decision)
        {
            if (decision.GetSimplifiedConditionsList().Count > 31)
                throw new ArgumentException("This implementation handles up to 31 conditions only");
            Dictionary<string, List<MCDCConditionRowPair>> conditionsTruthTablePairs = FindAllPairOfInterestsPerCondition(decision);

            List<PossibleMCDCCombination> mcdcAlternatives = new List<PossibleMCDCCombination>();
            GeneratePossibleMCDCAlternatives(mcdcAlternatives, null, conditionsTruthTablePairs.Values.ToList(), 0);

            if (UseDontCaresToMinimize)
            {
                mcdcAlternatives.Sort();
                List<PossibleMCDCCombination> minimizedMCDCAlternatives = new List<PossibleMCDCCombination>() { mcdcAlternatives[0] };
                for (int j = 1; j < mcdcAlternatives.Count; j++)
                    if (mcdcAlternatives[j].IsMatchConsideringDontCares(mcdcAlternatives[0]))
                        NumberOfSavedMCDCTargetsFromDontCares += mcdcAlternatives[j].MCDCRows.Count;
                    else
                        minimizedMCDCAlternatives.Add(mcdcAlternatives[j]);
                mcdcAlternatives = minimizedMCDCAlternatives;
            }
            return mcdcAlternatives;
        }

        private void GeneratePossibleMCDCAlternatives(List<PossibleMCDCCombination> outputRowsList, PossibleMCDCCombination newlyCombinedRows, List<List<MCDCConditionRowPair>> listOfPairsList, int currentPairsList)
        {
            if (null != newlyCombinedRows)
                newlyCombinedRows.MCDCRows.Sort();

            if (outputRowsList.Count >= MaxNumberOfMCDCTestAlternativesToGenerate)
                return;
            if (currentPairsList == listOfPairsList.Count)
            {
                bool foundSimilarMCDCCombination = false;
                if (UseDontCaresToMinimize)
                {
                    for (int k = 0; k < outputRowsList.Count; k++)
                        if (outputRowsList[k].IsMatchConsideringDontCares(newlyCombinedRows))
                        {
                            foundSimilarMCDCCombination = true;
                            if (newlyCombinedRows.CompareTo(outputRowsList[k]) > 0)
                                outputRowsList[k] = newlyCombinedRows;
                            break;
                        }
                }
                else
                {
                    for (int k = 0; k < outputRowsList.Count; k++)
                        if (0 == outputRowsList[k].CompareTo(newlyCombinedRows))
                        {
                            foundSimilarMCDCCombination = true;
                            break;
                        }
                }
                if (!foundSimilarMCDCCombination)
                    outputRowsList.Add(newlyCombinedRows);
                return;
            }

            List<MCDCConditionRowPair> firstPairsList = listOfPairsList.ElementAt(currentPairsList);

            List<int> remainingIndices = new List<int>();
            for (int i = 0; i < firstPairsList.Count; i++)
                remainingIndices.Add(i);

            while (remainingIndices.Count > 0)
            {

                int i = UseDontCaresToMinimize ? remainingIndices[0] : remainingIndices.ElementAt(new ThreadSafeRandom().Next(0, remainingIndices.Count));
                remainingIndices.Remove(i);

                if (null != newlyCombinedRows)
                {
                    PossibleMCDCCombination possibleMCDCTestCase = new PossibleMCDCCombination(new List<TruthTableRow>(newlyCombinedRows.MCDCRows));
                    bool foundFirstRowInPair = false, foundSecondRowInPair = false;
                    if (UseDontCaresToMinimize)
                    {
                        for (int j = 0; (!foundFirstRowInPair || !foundSecondRowInPair) && j < newlyCombinedRows.MCDCRows.Count; j++)
                        {
                            if (newlyCombinedRows.MCDCRows[j].IsTruthTableRowEquivalentConsideringDontCares(firstPairsList[i].FirstRow, true))
                            {
                                foundFirstRowInPair = true;
                                if (firstPairsList[i].FirstRow.CompareToUsingDontCares(possibleMCDCTestCase.MCDCRows[j]) < 0)
                                    possibleMCDCTestCase.MCDCRows[j] = firstPairsList[i].FirstRow;
                            }
                            else if (newlyCombinedRows.MCDCRows[j].IsTruthTableRowEquivalentConsideringDontCares(firstPairsList[i].SecondRow, true))
                            {
                                foundSecondRowInPair = true;
                                if (firstPairsList[i].SecondRow.CompareToUsingDontCares(possibleMCDCTestCase.MCDCRows[j]) < 0)
                                    possibleMCDCTestCase.MCDCRows[j] = firstPairsList[i].SecondRow;
                            }
                        }
                    }
                    else
                    {
                        if (newlyCombinedRows.MCDCRows.Contains(firstPairsList[i].FirstRow))
                            foundFirstRowInPair = true;
                        if (newlyCombinedRows.MCDCRows.Contains(firstPairsList[i].SecondRow))
                            foundSecondRowInPair = true;
                    }
                    if (!foundFirstRowInPair)
                        possibleMCDCTestCase.MCDCRows.Add(firstPairsList[i].FirstRow);
                    if (!foundSecondRowInPair)
                        possibleMCDCTestCase.MCDCRows.Add(firstPairsList[i].SecondRow);

                    GeneratePossibleMCDCAlternatives(outputRowsList, possibleMCDCTestCase, listOfPairsList, 1 + currentPairsList);
                }
                else
                {
                    PossibleMCDCCombination possibleMCDCCombination = new PossibleMCDCCombination();
                    possibleMCDCCombination.MCDCRows.Add(firstPairsList[i].FirstRow);
                    possibleMCDCCombination.MCDCRows.Add(firstPairsList[i].SecondRow);
                    GeneratePossibleMCDCAlternatives(outputRowsList, possibleMCDCCombination, listOfPairsList, 1 + currentPairsList);
                }
            }
        }

        private Dictionary<string, List<MCDCConditionRowPair>> FindAllPairOfInterestsPerCondition(Decision decision)
        {
            string[] conditions = decision.GetSimplifiedConditionsList().ToArray();
            Dictionary<string, List<MCDCConditionRowPair>> conditionsTruthTablePairs = new Dictionary<string, List<MCDCConditionRowPair>>();
            TruthTableRow[] truthTableWithOutput = decision.GenerateTruthTable();
            List<int> duplicateConditionsIndeces = new List<int>();
            truthTableWithOutput = DetectNonFeasibleTruthTableRowsAndDuplicateConditions(decision.ConditionsMap, truthTableWithOutput, duplicateConditionsIndeces);

            int numberOfRepeatedConditionsPassed = 0;
            for (int i = conditions.Length - 1; i >= 0; i--)
            {
                if (duplicateConditionsIndeces.Contains(i))
                {
                    numberOfRepeatedConditionsPassed++;
                    continue;
                }

                conditionsTruthTablePairs.Add(conditions[i], new List<MCDCConditionRowPair>());
                List<int> secondRowsInPairConsumed = new List<int>();
                for (int j = 0, numberOfConsumedRows = 0; numberOfConsumedRows != truthTableWithOutput.Length; j++)
                {
                    if (secondRowsInPairConsumed.Contains(j))
                        continue;
                    int secondRowIndex = j + (int)Math.Pow(2, conditions.Length - 1 - i - numberOfRepeatedConditionsPassed);
                    MCDCConditionRowPair pair = new MCDCConditionRowPair(truthTableWithOutput[j], truthTableWithOutput[secondRowIndex]);
                    numberOfConsumedRows += 2;
                    secondRowsInPairConsumed.Add(secondRowIndex);
                    if (truthTableWithOutput[j].Output != truthTableWithOutput[secondRowIndex].Output)
                        conditionsTruthTablePairs[conditions[i]].Add(pair);
                }
                if (UseDontCaresToMinimize)
                    conditionsTruthTablePairs[conditions[i]].Sort();
            }
            return conditionsTruthTablePairs;
        }

        private TruthTableRow[] DetectNonFeasibleTruthTableRowsAndDuplicateConditions(Dictionary<string, List<int>> conditionsMap, TruthTableRow[] truthTable, List<int> duplicateConditionsIndices)
        {
            if (conditionsMap == null)
                conditionsMap = new Dictionary<string, List<int>>();

            int numberOfFeasibleRows = truthTable.Length;
            for (int i = 0; i < conditionsMap.Count; i++)
            {
                if (conditionsMap.Values.ElementAt(i).Count > 1)
                {
                    List<int> duplicateConditions = conditionsMap.Values.ElementAt(i);
                    duplicateConditionsIndices.AddRange(duplicateConditions.Skip(1));
                    for (int j = 0; j < truthTable.Length; j++)
                    {
                        for (int k = 1; null != truthTable[j] && k < duplicateConditions.Count; k++)
                            if (!truthTable[j].IsConditionsValuesEqual(duplicateConditions.ElementAt(0), duplicateConditions.ElementAt(k)))
                            {
                                truthTable[j] = null;
                                numberOfFeasibleRows--;
                            }
                    }
                }
            }

            if (numberOfFeasibleRows != truthTable.Length)
            {
                TruthTableRow[] newTruthTable = new TruthTableRow[numberOfFeasibleRows];
                for (int i = 0, j = 0; i < truthTable.Length; i++)
                    if (null != truthTable[i])
                        newTruthTable[j++] = truthTable[i];
                truthTable = newTruthTable;
            }
            return truthTable;
        }
    }
}
