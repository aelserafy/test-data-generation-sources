﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;

namespace DecisionsModel.TestTargetsGenerators
{
    class MCDCConditionRowPair : IComparable<MCDCConditionRowPair>, IEquatable<MCDCConditionRowPair>, IEqualityComparer<MCDCConditionRowPair>
    {
        public TruthTableRow FirstRow { get; private set; }
        public TruthTableRow SecondRow { get; private set; }

        public MCDCConditionRowPair(TruthTableRow firstRow, TruthTableRow secondRow)
        {
            if (firstRow.RowIndex < secondRow.RowIndex)
            {
                FirstRow = firstRow;
                SecondRow = secondRow;
            }
            else
            {
                FirstRow = secondRow;
                SecondRow = firstRow;
            }
        }

        public int CompareTo(MCDCConditionRowPair other)
        {
            if (Equals(other))
                return 0;
            int dontCaresCount = FirstRow.GetDontCaresCount() + SecondRow.GetDontCaresCount(),
                dontCaresCountInOther = other.FirstRow.GetDontCaresCount()+ other.SecondRow.GetDontCaresCount(),
                dontCaresSum = FirstRow.DontCaresMask + SecondRow.DontCaresMask,
                dontCaresSumInOther = other.FirstRow.DontCaresMask + other.SecondRow.DontCaresMask,
                rowIndeciesSum = FirstRow.RowIndex + SecondRow.RowIndex,
                rowIndeciesSumInOther = other.FirstRow.RowIndex + other.SecondRow.RowIndex;

            if (dontCaresCount > dontCaresCountInOther || dontCaresSum > dontCaresSumInOther ||
                rowIndeciesSum < rowIndeciesSumInOther)
                return -1;
            return 1;
        }

        public bool Equals(MCDCConditionRowPair other)
        {
            return FirstRow.Equals(other.FirstRow) && SecondRow.Equals(other.SecondRow);

        }

        public bool Equals(MCDCConditionRowPair x, MCDCConditionRowPair y)
        {
            return x.Equals(y);
        }

        public override bool Equals(object obj)
        {
            if (obj is MCDCConditionRowPair)
                return Equals((MCDCConditionRowPair) obj);
            return false;
        }

        public override int GetHashCode()
        {
            return FirstRow.GetHashCode() + SecondRow.GetHashCode();
        }

        public int GetHashCode(MCDCConditionRowPair obj)
        {
            return obj.GetHashCode();
        }
    }
}
