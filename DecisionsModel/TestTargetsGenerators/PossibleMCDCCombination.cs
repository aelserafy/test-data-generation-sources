﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;

namespace DecisionsModel.TestTargetsGenerators
{
    public class PossibleMCDCCombination : IComparable<PossibleMCDCCombination>
    {
        public List<TruthTableRow> MCDCRows { get; private set; }

        public PossibleMCDCCombination()
        {
            MCDCRows = new List<TruthTableRow>();
        }

        public PossibleMCDCCombination(List<TruthTableRow> mcdcRows)
        {
            MCDCRows = mcdcRows;
        }

        public string GetTruthTableRowsText(int numberOfConditions, int numberOfTabs)
        {
            string formatedTableRows = "";
            foreach (TruthTableRow row in MCDCRows)
            {
                for (int i = 0; i < numberOfTabs; i++)
                    formatedTableRows += "\t";
                formatedTableRows += row.GetRowText() + Environment.NewLine;
            }
            return formatedTableRows;
        }

        public int CompareTo(PossibleMCDCCombination other)
        {
            if (MCDCRows.Count < other.MCDCRows.Count)
                return -1;
            if (MCDCRows.Count > other.MCDCRows.Count)
                return 1;

            foreach (TruthTableRow singleRow in MCDCRows)
                if (!other.MCDCRows.Contains(singleRow))
                {
                    int dontCaresCount = 0,
                        dontCaresCountInOther = 0,
                        dontCaresSum = 0,
                        dontCaresSumInOther = 0,
                        rowIndicesSum = 0,
                        rowIndicesSumInOther = 0;
                    foreach (TruthTableRow singleRow1 in MCDCRows)
                    {
                        dontCaresCount += singleRow1.GetDontCaresCount();
                        dontCaresSum += singleRow1.DontCaresMask;
                        rowIndicesSum += singleRow1.RowIndex;
                    }
                    foreach (TruthTableRow singleRow1InOther in other.MCDCRows)
                    {
                        dontCaresCountInOther += singleRow1InOther.GetDontCaresCount();
                        dontCaresSumInOther += singleRow1InOther.DontCaresMask;
                        rowIndicesSumInOther += singleRow1InOther.RowIndex;
                    }
                    if (dontCaresCount > dontCaresCountInOther || dontCaresSum > dontCaresSumInOther || rowIndicesSum < rowIndicesSumInOther)
                        return -1;
                    return 1;
                }
            return 0;
        }

        public bool IsMatchConsideringDontCares(PossibleMCDCCombination other)
        {
            for (int i = 0; i < MCDCRows.Count; i++)
            {
                bool eqFound = false;
                for (int j = 0; !eqFound && j < other.MCDCRows.Count; j++)
                    if (MCDCRows[i].IsTruthTableRowEquivalentConsideringDontCares(other.MCDCRows[j], true))
                        eqFound = true;
                if (!eqFound)
                    return false;
            }
            return true;
        }
    }
}
