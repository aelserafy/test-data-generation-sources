﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System;
using System.Collections.Generic;

namespace DecisionsModel
{
    public class TruthTableRow : IComparable<TruthTableRow>, IEquatable<TruthTableRow>, IEqualityComparer<TruthTableRow>
    {
        public int RowIndex { get; private set; }
        public int DontCaresMask { get; private set; }
        public byte NumberOfConditions { get; private set; }
        public bool Output { get; private set; }

        public TruthTableRow(byte numberOfConditions, int rowIndex, int dontCaresMask, bool output)
        {
            if (rowIndex < 0 || dontCaresMask < 0)
                throw new ArgumentException("rowIndex & dontCaresMask must be >= 0 ");
            if (numberOfConditions > 31)
                throw new ArgumentException("This implementation handles up to 31 conditions only");
            RowIndex = rowIndex;
            DontCaresMask = dontCaresMask;
            NumberOfConditions = numberOfConditions;
            Output = output;
        }

        public void InvertOutput()
        {
            Output = !Output;
        }

        /// <summary>
        /// Get the boolean value of a certain condition
        /// </summary>
        /// <param name="conditionIndex">Zero-based index of the condition</param>
        /// <returns>Boolean value of the condition</returns>
        public bool GetConditionValue(int conditionIndex)
        {
            return DecodeBoolean(RowIndex, conditionIndex);
        }

        /// <summary>
        /// Get the don't care flag value for a certain condition
        /// </summary>
        /// <param name="conditionIndex">Zero-based index of the condition</param>
        /// <returns>Boolean don't care flag value of the condition</returns>
        public bool GetDontCareMaskValue(int conditionIndex)
        {
            return DecodeBoolean(DontCaresMask, conditionIndex);
        }

        private bool DecodeBoolean(int value, int conditionIndex)
        {
            //Conditions are parsed from code in LTR order, while the Truth Table encoding used is in RTL order
            //To unify the "condition index" term in code, LTR order is used. Zero based.
            return ((value >> (NumberOfConditions - 1 - conditionIndex)) & 1) == 1;
        }

        public int GetDontCaresCount()
        {
            int dontCaresCount = 0;
            for(int i = 0; i < NumberOfConditions; i++)
                if (GetDontCareMaskValue(i))
                    dontCaresCount++;
            return dontCaresCount;
        }

        private bool CheckIfNumberOfConditionsIsEqual(TruthTableRow other)
        {
            if (NumberOfConditions != other.NumberOfConditions)
                throw new Exception("Comparision between truth table rows that don't belong to the same truth table!!!");
            return true;
        }

        public int CompareTo(TruthTableRow other)
        {
            CheckIfNumberOfConditionsIsEqual(other);
            return RowIndex.CompareTo(other.RowIndex);
        }

        public int CompareToUsingDontCares(TruthTableRow other)
        {
            CheckIfNumberOfConditionsIsEqual(other);
            int dontCaresCount = other.GetDontCaresCount().CompareTo(GetDontCaresCount());
            if (dontCaresCount != 0)
                return dontCaresCount;
            int dontCaresComparison = other.DontCaresMask.CompareTo(DontCaresMask);
            if (dontCaresComparison != 0)
                return dontCaresComparison;
            return CompareTo(other);
        }

        public bool IsConditionsValuesEqual(int firstConditionIndex, int secondConditionIndex)
        {
            return GetConditionValue(firstConditionIndex) == GetConditionValue(secondConditionIndex);
        }

        public Condition[] GetRowValues()
        {
            Condition[] row = new Condition[NumberOfConditions];
            for (int i = 0; i < NumberOfConditions; i++)
            {
                bool value = GetConditionValue(i);
                bool dontCare = GetDontCareMaskValue(i);
                row[i] = new Condition(value, dontCare);
            }
            return row;
        }

        public string GetRowText()
        {
            string rowText = "";
            Condition[] rowTokens = GetRowValues();
            foreach (Condition tok in rowTokens)
            {
                if (tok.ConditionDontCareValue)
                    rowText += "X       ";
                else
                    if (tok.ConditionBooleanValue)
                        rowText += "true    ";
                    else
                        rowText += "false   ";
                for (int i = 0; i < 3; i++)
                    rowText += "        ";
            }
            return rowText.Trim();
        }

        public bool IsTruthTableRowEquivalentConsideringDontCares(TruthTableRow truthTableRow, bool considerDontCaresAndOutput)
        {
            CheckIfNumberOfConditionsIsEqual(truthTableRow);
            if (considerDontCaresAndOutput && (DontCaresMask != truthTableRow.DontCaresMask || Output != truthTableRow.Output))
                return false;
            int rowsInEqualities = RowIndex ^ truthTableRow.RowIndex;
            if ((~DontCaresMask & rowsInEqualities) > 0)
                return false;
            return true;
        }

        public bool Equals(TruthTableRow other)
        {
            return CheckIfNumberOfConditionsIsEqual(other) && RowIndex == other.RowIndex && DontCaresMask == other.DontCaresMask && Output == other.Output;
        }

        public override bool Equals(object obj)
        {
            if (obj is TruthTableRow)
                return Equals((TruthTableRow)obj);
            return false;
        }

        public bool Equals(TruthTableRow x, TruthTableRow y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode()
        {
            return RowIndex;
        }

        public int GetHashCode(TruthTableRow obj)
        {
            return obj.GetHashCode();
        }
    }
}
