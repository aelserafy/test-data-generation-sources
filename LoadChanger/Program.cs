﻿using System;
using System.IO;
using System.Threading;

namespace LoadChanger
{
    class Program
    {
        static void Main(string[] args)
        {
            string cpu_load = args[0];
            bool lowLoad = File.ReadAllText(cpu_load) == "0.3";
            while (true)
            {
                if (lowLoad && DateTime.Now.TimeOfDay > new TimeSpan(21, 00, 00))
                {
                    while (lowLoad)
                    {
                        try
                        {
                            File.WriteAllText(cpu_load, "0.9");
                            Console.WriteLine(DateTime.Now + ": now 0.9");
                            lowLoad = false;
                        }
                        catch (Exception ex) { }
                    }
                }
                else if (!lowLoad && DateTime.Now.TimeOfDay > new TimeSpan(08, 00, 00))
                {
                    while (!lowLoad)
                    {
                        try
                        {
                            File.WriteAllText(cpu_load, "0.3");
                            Console.WriteLine(DateTime.Now + ": now 0.3");
                            lowLoad = true;
                        }
                        catch (Exception ex) { }
                    }
                }
                Thread.Sleep(1000 * 60 * 10);
            }
        }
    }
}
