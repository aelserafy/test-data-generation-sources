﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;
using DecisionsModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestingProject
{
    [TestClass()]
    public class DecisionTests
    {
        [TestMethod()]
        public void GetTTRowsThatCausesThisConditionToEvaluateTests()
        {
            Condition x = Condition.GetDummyCondition(0);
            Condition y = Condition.GetDummyCondition(1);
            Condition z = Condition.GetDummyCondition(2);
            Condition xx = Condition.GetDummyCondition(3);

            // (x || y) && z && xx
            Decision dec1 = new Decision(x, y, LogicalOperatorType.Or);
            Decision dec2 = new Decision(dec1, z, LogicalOperatorType.And);
            Decision dec3 = new Decision(dec2, xx, LogicalOperatorType.And);
            List<TruthTableRow> actual = dec3.GetTTRowsThatCausesThisConditionToEvaluate(z, false);
            List<TruthTableRow> expected = new List<TruthTableRow>() { new TruthTableRow(4, 6, 0, false), new TruthTableRow(4, 7, 0, true), new TruthTableRow(4, 10, 4, false), new TruthTableRow(4, 11, 4, true), new TruthTableRow(4, 14, 4, false), new TruthTableRow(4, 15, 4, true) };
            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
                Assert.AreEqual(expected[i], actual[i]);

            actual = dec3.GetTTRowsThatCausesThisConditionToEvaluate(z, true);
            expected = new List<TruthTableRow>() { new TruthTableRow(4, 10, 5, false), new TruthTableRow(4, 6, 1, false) };
            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
                Assert.AreEqual(expected[i], actual[i]);

            // (x && y || z) && xx
            dec1 = new Decision(x, y, LogicalOperatorType.And);
            dec2 = new Decision(dec1, z, LogicalOperatorType.Or);
            dec3 = new Decision(dec2, xx, LogicalOperatorType.And);
            actual = dec3.GetTTRowsThatCausesThisConditionToEvaluate(z, false);
            expected = new List<TruthTableRow>() { new TruthTableRow(4, 2, 4, false), new TruthTableRow(4, 3, 4, true), new TruthTableRow(4, 6, 4, false), new TruthTableRow(4, 7, 4, true), new TruthTableRow(4, 10, 0, false), new TruthTableRow(4, 11, 0, true) };
            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
                Assert.AreEqual(expected[i], actual[i]);

            actual = dec3.GetTTRowsThatCausesThisConditionToEvaluate(z, true);
            expected = new List<TruthTableRow>() { new TruthTableRow(4, 2, 5, false), new TruthTableRow(4, 10, 1, false) };
            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
                Assert.AreEqual(expected[i], actual[i]);
        }

        [TestMethod()]
        public void GetSimplifiedConditionsListTest()
        {
            Condition leftCondition = Condition.GetDummyCondition(5);
            leftCondition.Text = "x < 10";

            Condition rightCondition = Condition.GetDummyCondition(10);
            rightCondition.Text = "x > 15";

            Decision decision1 = new Decision(leftCondition, rightCondition);

            List<string> conditions = decision1.GetSimplifiedConditionsList();
            Assert.AreEqual(conditions.Count, 2);
            Assert.AreEqual(conditions[0], "Condition_5");
            Assert.AreEqual(conditions[1], "Condition_10");

            leftCondition = Condition.GetDummyCondition(15);
            leftCondition.Text = "x < 10";

            rightCondition = Condition.GetDummyCondition(20);
            rightCondition.Text = "x > 15";

            Decision decision2 = new Decision(leftCondition, rightCondition);

            Decision decision3 = new Decision(decision1, decision2);

            conditions = decision3.GetSimplifiedConditionsList();
            Assert.AreEqual(4, conditions.Count);
            Assert.AreEqual("Condition_5", conditions[0]);
            Assert.AreEqual("Condition_10", conditions[1]);
            Assert.AreEqual("Condition_15", conditions[2]);
            Assert.AreEqual("Condition_20", conditions[3]);
        }

        [TestMethod()]
        public void GenerateTruthTableTest1()
        {
            // Testing !(A && B) 
            Condition condition1 = Condition.GetDummyCondition(1); //A
            condition1.Text = "x < 10";
            Decision decision3 = new Decision(condition1); //A


            Condition condition2 = Condition.GetDummyCondition(2); //B
            condition2.Text = "y > 5";
            Decision decision4 = new Decision(condition2); //A


            Decision decision2 = new Decision(decision3, decision4, LogicalOperatorType.And); //( && )
            Decision decision1 = new Decision(decision2, LogicalOperatorType.Not); //!()

            TruthTableRow[] truthTable = decision1.GenerateTruthTable();
            Assert.AreEqual(4, truthTable.Length);
            Assert.AreEqual(0, truthTable[0].RowIndex);
            Assert.AreEqual(1, truthTable[1].RowIndex);
            Assert.AreEqual(2, truthTable[2].RowIndex);
            Assert.AreEqual(3, truthTable[3].RowIndex);
            Assert.IsTrue(truthTable[0].Output);
            Assert.IsTrue(truthTable[1].Output);
            Assert.IsTrue(truthTable[2].Output);
            Assert.IsFalse(truthTable[3].Output);
        }

        [TestMethod()]
        public void GenerateTruthTableTest2()
        {
            // Testing (A && B) 
            Condition condition1 = Condition.GetDummyCondition(1); //A
            Condition condition2 = Condition.GetDummyCondition(2); //B

            Decision decision2 = new Decision(condition1); //A
            Decision decision3 = new Decision(condition2); //A

            Decision decision1 = new Decision(decision2, decision3, LogicalOperatorType.And); //( && )

            condition1.Text = "x < 10";
            condition2.Text = "y > 5";

            TruthTableRow[] truthTable = decision1.GenerateTruthTable();
            Assert.AreEqual(4, truthTable.Length);
            Assert.AreEqual(0, truthTable[0].RowIndex);
            Assert.AreEqual(1, truthTable[1].RowIndex);
            Assert.AreEqual(2, truthTable[2].RowIndex);
            Assert.AreEqual(3, truthTable[3].RowIndex);
            Assert.IsFalse(truthTable[0].Output);
            Assert.IsFalse(truthTable[1].Output);
            Assert.IsFalse(truthTable[2].Output);
            Assert.IsTrue(truthTable[3].Output);
        }

        [TestMethod()]
        public void GenerateTruthTableTest3()
        {
            // Testing (!A && !B) 
            Condition condition1 = Condition.GetDummyCondition(1); //A
            Condition condition2 = Condition.GetDummyCondition(2); //B

            Decision decision2 = new Decision(condition1, LogicalOperatorType.Not); //A
            Decision decision3 = new Decision(condition2, LogicalOperatorType.Not); //A
            Decision decision1 = new Decision(decision2, decision3, LogicalOperatorType.And); //( && )

            condition1.Text = "x < 10";
            condition2.Text = "y > 5";

            TruthTableRow[] truthTable = decision1.GenerateTruthTable();
            Assert.AreEqual(4, truthTable.Length);
            Assert.AreEqual(0, truthTable[0].RowIndex);
            Assert.AreEqual(1, truthTable[1].RowIndex);
            Assert.AreEqual(2, truthTable[2].RowIndex);
            Assert.AreEqual(3, truthTable[3].RowIndex);
            Assert.IsTrue(truthTable[0].Output);
            Assert.IsFalse(truthTable[1].Output);
            Assert.IsFalse(truthTable[2].Output);
            Assert.IsFalse(truthTable[3].Output);
        }

        [TestMethod()]
        public void GenerateTruthTableTest4()
        {
            // Testing (!A || !B) 
            Condition condition1 = Condition.GetDummyCondition(1); //A
            Condition condition2 = Condition.GetDummyCondition(2); //B

            Decision decision2 = new Decision(condition1, LogicalOperatorType.Not); //A
            Decision decision3 = new Decision(condition2, LogicalOperatorType.Not); //A
            Decision decision1 = new Decision(decision2, decision3, LogicalOperatorType.Or); //( && )

            condition1.Text = "x < 10";
            condition2.Text = "y > 5";

            TruthTableRow[] truthTable = decision1.GenerateTruthTable();
            Assert.AreEqual(4, truthTable.Length);
            Assert.AreEqual(0, truthTable[0].RowIndex);
            Assert.AreEqual(1, truthTable[1].RowIndex);
            Assert.AreEqual(2, truthTable[2].RowIndex);
            Assert.AreEqual(3, truthTable[3].RowIndex);
            Assert.IsTrue(truthTable[0].Output);
            Assert.IsTrue(truthTable[1].Output);
            Assert.IsTrue(truthTable[2].Output);
            Assert.IsFalse(truthTable[3].Output);
        }

        [TestMethod()]
        public void GenerateTruthTableTest5()
        {
            // Testing ((A && B) || (C && D)) 
            Condition condition1 = Condition.GetDummyCondition(1);
            Condition condition2 = Condition.GetDummyCondition(2);
            Condition condition3 = Condition.GetDummyCondition(3);
            Condition condition4 = Condition.GetDummyCondition(4);

            Decision decision2 = new Decision(condition1, condition2, LogicalOperatorType.And);
            Decision decision3 = new Decision(condition3, condition4, LogicalOperatorType.And);
            Decision decision1 = new Decision(decision2, decision3, LogicalOperatorType.Or); //( && )

            TruthTableRow[] truthTable = decision1.GenerateTruthTable();
            Assert.AreEqual(16, truthTable.Length);
            Assert.AreEqual(0, truthTable[0].RowIndex);
            Assert.AreEqual(1, truthTable[1].RowIndex);
            Assert.AreEqual(2, truthTable[2].RowIndex);
            Assert.AreEqual(3, truthTable[3].RowIndex);
            Assert.AreEqual(4, truthTable[4].RowIndex);
            Assert.AreEqual(5, truthTable[5].RowIndex);
            Assert.AreEqual(6, truthTable[6].RowIndex);
            Assert.AreEqual(7, truthTable[7].RowIndex);
            Assert.AreEqual(8, truthTable[8].RowIndex);
            Assert.AreEqual(9, truthTable[9].RowIndex);
            Assert.AreEqual(10, truthTable[10].RowIndex);
            Assert.AreEqual(11, truthTable[11].RowIndex);
            Assert.AreEqual(12, truthTable[12].RowIndex);
            Assert.AreEqual(13, truthTable[13].RowIndex);
            Assert.AreEqual(14, truthTable[14].RowIndex);
            Assert.AreEqual(15, truthTable[15].RowIndex);
            Assert.IsFalse(truthTable[0].Output);
            Assert.IsFalse(truthTable[1].Output);
            Assert.IsFalse(truthTable[2].Output);
            Assert.IsTrue(truthTable[3].Output);
            Assert.IsFalse(truthTable[4].Output);
            Assert.IsFalse(truthTable[5].Output);
            Assert.IsFalse(truthTable[6].Output);
            Assert.IsTrue(truthTable[7].Output);
            Assert.IsFalse(truthTable[8].Output);
            Assert.IsFalse(truthTable[9].Output);
            Assert.IsFalse(truthTable[10].Output);
            Assert.IsTrue(truthTable[11].Output);
            Assert.IsTrue(truthTable[12].Output);
            Assert.IsTrue(truthTable[13].Output);
            Assert.IsTrue(truthTable[14].Output);
            Assert.IsTrue(truthTable[15].Output);
        }

        [TestMethod()]
        public void GetReconstructedTextTest()
        {
            // Testing !(A && B) 
            Condition condition1 = Condition.GetDummyCondition(1); //A
            condition1.Text = "x < 10";


            Condition condition2 = Condition.GetDummyCondition(2); //B
            condition2.Text = "y > 5";

            Decision decision4 = new Decision(condition2); //A
            Decision decision3 = new Decision(condition1); //A
            Decision decision2 = new Decision(decision3, decision4, LogicalOperatorType.And); //( && )
            Decision decision1 = new Decision(decision2, LogicalOperatorType.Not); //!()

            string decisionText = decision1.GetReconstructedText();
            Assert.AreEqual("!(x < 10 && y > 5)", decisionText);
        }
    }
}
