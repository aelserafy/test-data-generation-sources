﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomGenetic;
using CustomGenetic.Benchmarks;
using CustomGenetic.GAOperators;
using DecisionsModel;

namespace TestingProject
{
    [TestClass]
    public class FitnessEvaluationTests : EqualityCompartor
    {
        [TestMethod]
        public void NextDateWithIfFitnessEvaluationUsingIntTest()
        {
            Environment nextDateEnv = new NextDateEnvWithIf(false);
            nextDateEnv.SetTargetDataType(false);
            nextDateEnv.SetRunConfigurations(false, false, false, true);

            foreach (Decision decision in nextDateEnv.AppDecisions)
                decision.GenerateMCDCTestAlternatives(nextDateEnv.MaxNumberOfMCDCTestAlternativesToGenerate, true);

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[0];
            MyFitness fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[0], nextDateEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 2010 }));
            //nextDateEnv.TargetTestCase = nextDateEnv.TargetDecision.MCDCAlternatives[0].MCDCRows[1];
            //CheckEquality(0, fitness.Evaluate(new int[] { 0, 0, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[0], nextDateEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 3010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[0], nextDateEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[3]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 1000 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 10, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 13, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[2];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 1, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[3];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 3, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[4];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 5, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[5];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 7, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[6];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[6], nextDateEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[6], nextDateEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 8, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[7];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[7], nextDateEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[7], nextDateEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 10, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[8];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[8], nextDateEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[8], nextDateEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 12, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[9];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[9], nextDateEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 4, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[9], nextDateEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 2874 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[10];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[10], nextDateEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[10], nextDateEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[1]);
            CheckNonEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            nextDateEnv.SetRunConfigurations(false, true, false, true);
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[10], nextDateEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2800 }));
            nextDateEnv.SetRunConfigurations(false, false, false, true);
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[10], nextDateEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2100 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[10], nextDateEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[3]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2001 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[11];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[11], nextDateEnv.AppDecisions[11].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 5, 2, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[11], nextDateEnv.AppDecisions[11].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 60, 2, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[11], nextDateEnv.AppDecisions[11].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[12];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 1, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 1, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[13];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[13], nextDateEnv.AppDecisions[13].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 12, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[13], nextDateEnv.AppDecisions[13].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 10, 2011 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 8, 2565 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 3, 2333 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 3, 2137 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 10, 2033 }));

            //Diverging nodes fitness tests
            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[13];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[13], nextDateEnv.AppDecisions[13].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(2 + NormalizeBD(32), fitness.Evaluate(new[] { 60, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[9];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[9], nextDateEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(2 + NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 0, 10, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[11];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[11], nextDateEnv.AppDecisions[11].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 0, 13, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[12];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 0, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[4];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(3 + NormalizeBD(1), fitness.Evaluate(new[] { 0, 0, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 1923, 5546, 2999 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 7483, 5791, 2999 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[9];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[9], nextDateEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2), fitness.Evaluate(new[] { 1, 4, 2021 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[13];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[13], nextDateEnv.AppDecisions[13].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2), fitness.Evaluate(new[] { 31, 10, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[12];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 31, 1, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[11];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[11], nextDateEnv.AppDecisions[11].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(1), fitness.Evaluate(new[] { 0, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[9];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[9], nextDateEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2), fitness.Evaluate(new[] { 0, 4, 2001 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(NormalizeBD(3 + nextDateEnv.BDConstant), fitness.Evaluate(new[] { 0, 4, 2010 }));
        }

        [TestMethod]
        public void NextDateWithIfFitnessEvaluationUsingDoubleTest()
        {
            Environment nextDateEnv = new NextDateEnvWithIf(true);
            nextDateEnv.SetTargetDataType(true);
            nextDateEnv.SetRunConfigurations(false, false, false, true);

            foreach (Decision decision in nextDateEnv.AppDecisions)
                decision.GenerateMCDCTestAlternatives(nextDateEnv.MaxNumberOfMCDCTestAlternativesToGenerate, true);

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[2];
            MyFitness fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 1.5, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[5];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 7.5, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[12];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 1.5, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 1.5, 2011 }));

            //Diverging nodes fitness tests
            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[4];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(3 + NormalizeBD(1.5), fitness.Evaluate(new[] { 0, 0, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[12];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[12], nextDateEnv.AppDecisions[12].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 31, 1.5, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2.5 + nextDateEnv.BDConstant), fitness.Evaluate(new[] { 0, 4, 2010 }));
        }

        [TestMethod]
        public void NextDateWithSwitchFitnessEvaluationTest()
        {
            Environment nextDateEnv = new NextDateEnvWithSwitch();
            nextDateEnv.SetTargetDataType(false);
            nextDateEnv.SetRunConfigurations(false, false, false, true);
            MyFitness fitness;

            for (int i = 0; i < nextDateEnv.AppDecisions.Count; i++)
                nextDateEnv.AppDecisions[i].GenerateMCDCTestAlternatives(nextDateEnv.MaxNumberOfMCDCTestAlternativesToGenerate, true);

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[0];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[0], nextDateEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 2010 }));
            //nextDateEnv.TargetTestCase = nextDateEnv.TargetDecision.MCDCAlternatives[0].MCDCRows[1];
            //CheckEquality(0, fitness.Evaluate(new int[] { 0, 0, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[0], nextDateEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 3010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[0], nextDateEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[3]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 1000 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 10, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 13, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[2];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 12, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 10, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[3]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 8, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[4]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 7, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[5]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 5, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[6]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 3, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[7]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 1, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[3];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 4, 2010 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 2874 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[4];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[1]);
            CheckNonEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2010 }));
            nextDateEnv.SetRunConfigurations(false, true, false, true);
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2800 }));
            nextDateEnv.SetRunConfigurations(false, false, false, true);
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2100 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[4], nextDateEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[3]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2001 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[5];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 5, 2, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 60, 2, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[6];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[6], nextDateEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 1, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[6], nextDateEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 1, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[7];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[7], nextDateEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 12, 2011 }));
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[7], nextDateEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 10, 2011 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 8, 2565 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 3, 2333 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 3, 2137 }));
            CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 31, 10, 2033 }));

            //Diverging nodes fitness tests
            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[7];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[7], nextDateEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(2 + NormalizeBD(32), fitness.Evaluate(new[] { 60, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[3];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 0, 10, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[5];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 0, 13, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[6];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[6], nextDateEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 0, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[2];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[2], nextDateEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 0, 0, 2010 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 1923, 5546, 2999 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 7483, 5791, 2999 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[3];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2), fitness.Evaluate(new[] { 1, 4, 2021 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[7];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[7], nextDateEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2), fitness.Evaluate(new[] { 31, 10, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[6];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[6], nextDateEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(nextDateEnv.BDConstant), fitness.Evaluate(new[] { 31, 1, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[5];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[5], nextDateEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(NormalizeBD(1), fitness.Evaluate(new[] { 0, 2, 2011 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[3];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[3], nextDateEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(NormalizeBD(2), fitness.Evaluate(new[] { 0, 4, 2001 }));

            nextDateEnv.TargetDecision = nextDateEnv.AppDecisions[1];
            fitness = new MyFitness(nextDateEnv, nextDateEnv.AppDecisions[1], nextDateEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(NormalizeBD(3 + nextDateEnv.BDConstant), fitness.Evaluate(new[] { 0, 4, 2010 }));
        }

        [TestMethod]
        public void TriangleClassificationEnvFitnessEvaluationWithoutDataDependenciesTest()
        {
            for (int i = 0; i < 2; i++)
            {
                Environment triangleClassificationEnv = InitializeTriangleClassificationEnv(i != 0);

                MyFitness fitness;

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[0];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[0], triangleClassificationEnv.AppDecisions[0].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 0, 0, 0 }));
                //triangleClassificationEnv.TargetTestCase = triangleClassificationEnv.TargetDecision.MCDCAlternatives[0].MCDCRows[1];
                //CheckEquality(0, fitness.Evaluate(new int[] { 0, 0, 2010 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[1];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[1], triangleClassificationEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 1, 1 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[1], triangleClassificationEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 1, -1 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[1], triangleClassificationEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[2]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, -1, 1 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[1], triangleClassificationEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[3]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { -1, 1, 1 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[2];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[2], triangleClassificationEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[2], triangleClassificationEnv.AppDecisions[2].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 1, 3 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[3];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[3], triangleClassificationEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[3], triangleClassificationEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 2 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[4];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[4], triangleClassificationEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[4], triangleClassificationEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 1 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[5];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[5], triangleClassificationEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 1 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[5], triangleClassificationEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 3 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[6];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 4, 5 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 9, 2 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[2]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 9, 1, 2 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[3]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 1, 2, 9 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[7];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[7], triangleClassificationEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 3, 5 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[7], triangleClassificationEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 3, 3 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[8];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[8], triangleClassificationEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 5, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[8], triangleClassificationEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 3, 50 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[8], triangleClassificationEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[2]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 3, 5 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 5, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 50, 3, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[2]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 5, 3, 3 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[10];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[10], triangleClassificationEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 3, 7 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[10], triangleClassificationEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 50, 3 }));
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[10], triangleClassificationEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[2]);
                CheckEqualityWithExpectedInverse(0, fitness.Evaluate(new[] { 3, 5, 3 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[7];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[7], triangleClassificationEnv.AppDecisions[7].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(NormalizeBD(2 + triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 3, 3, 5 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[5];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[5], triangleClassificationEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[1]);
                CheckEqualityWithExpectedInverse(NormalizeBD(3), fitness.Evaluate(new[] { 1, 2, 1 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 5, 3, 3 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[4];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[4], triangleClassificationEnv.AppDecisions[4].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 2, 1 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[6];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
                CheckEqualityWithExpectedInverse(NormalizeBD(6), fitness.Evaluate(new[] { 1, 9, 2 }));

                triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[1];
                fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[1], triangleClassificationEnv.AppDecisions[1].MCDCAlternatives[0].MCDCRows[3]);
                CheckEqualityWithExpectedInverse(NormalizeBD(1 + triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 1, 1 }));
            }
        }

        [TestMethod]
        public void DivergingTriangleClassificationEnvFitnessEvaluationWithoutDataDependenciesTest()
        {
            Environment triangleClassificationEnv = InitializeTriangleClassificationEnv(false);
            MyFitness fitness;

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(2 + NormalizeBD(3), fitness.Evaluate(new[] { 3, 3, 3 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[6];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(3), fitness.Evaluate(new[] { 1, 2, 1 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[3];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[3], triangleClassificationEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { -1, 1, 1 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[10];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[10], triangleClassificationEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(4 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 2, 3 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[5];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[5], triangleClassificationEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 1, 1, -1 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[8];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[8], triangleClassificationEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(2 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 2, 3 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 5033, 5033, 748 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 3572, 3572, 1865 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 4632, 4632, 4089 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 7738, 7738, 6854 }));
        }

        [TestMethod]
        public void DivergingTriangleClassificationEnvFitnessEvaluationWithDataDependenciesTest()
        {
            Environment triangleClassificationEnv = InitializeTriangleClassificationEnv(true);
            MyFitness fitness;

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(3 + NormalizeBD(3) + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 3, 3, 3 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[6];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[6], triangleClassificationEnv.AppDecisions[6].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(2 + NormalizeBD(3) + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 2, 1 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[3];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[3], triangleClassificationEnv.AppDecisions[3].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { -1, 1, 1 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[10];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[10], triangleClassificationEnv.AppDecisions[10].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(6 + NormalizeBD(1) * 2 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 2, 3 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[5];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[5], triangleClassificationEnv.AppDecisions[5].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(1), fitness.Evaluate(new[] { 1, 1, -1 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[8];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[8], triangleClassificationEnv.AppDecisions[8].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(4 + NormalizeBD(1) * 2 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 1, 2, 3 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[1]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 5033, 5033, 748 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 3572, 3572, 1865 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[0]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 4632, 4632, 4089 }));

            triangleClassificationEnv.TargetDecision = triangleClassificationEnv.AppDecisions[9];
            fitness = new MyFitness(triangleClassificationEnv, triangleClassificationEnv.AppDecisions[9], triangleClassificationEnv.AppDecisions[9].MCDCAlternatives[0].MCDCRows[2]);
            CheckEqualityWithExpectedInverse(1 + NormalizeBD(triangleClassificationEnv.BDConstant), fitness.Evaluate(new[] { 7738, 7738, 6854 }));
        }

        private static Environment InitializeTriangleClassificationEnv(bool includeDataDependencies)
        {
            Environment triangleClassificationEnv = new TriangleClassificationEnv(includeDataDependencies);
            triangleClassificationEnv.SetTargetDataType(false);
            triangleClassificationEnv.SetRunConfigurations(false, false, false, true);

            for (int i = 0; i < triangleClassificationEnv.AppDecisions.Count; i++)
                triangleClassificationEnv.AppDecisions[i].GenerateMCDCTestAlternatives(triangleClassificationEnv.MaxNumberOfMCDCTestAlternativesToGenerate, true);
            return triangleClassificationEnv;
        }

        private double NormalizeBD(double rawBD)
        {
            return rawBD / (rawBD + 1);
        }

        private void CheckEqualityWithExpectedInverse(double x, double y)
        {
            if (x == 0)
                Assert.IsTrue(double.IsInfinity(y));
            else
                Assert.IsTrue(AreEqual(1 / x, y));
        }

        private void CheckNonEqualityWithExpectedInverse(double x, double y)
        {
            if (x == 0)
                Assert.IsFalse(double.IsInfinity(y));
            else
                Assert.IsFalse(AreEqual(1 / x, y));
        }
    }
}
