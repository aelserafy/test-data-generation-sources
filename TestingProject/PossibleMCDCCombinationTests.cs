﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using System.Collections.Generic;
using DecisionsModel;
using DecisionsModel.TestTargetsGenerators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestingProject
{
    [TestClass()]
    public class PossibleMCDCCombinationTests
    {
        /// <summary>
        ///A test for GenerateAllPossibleMCDCTestAlternatives
        ///</summary>
        [TestMethod()]
        public void GenerateAllPossibleMCDCTestAlternativesTestForUniqueCause()
        {
            Condition cond1 = Condition.GetDummyCondition(0);
            Condition cond2 = Condition.GetDummyCondition(1);
            Condition cond3 = Condition.GetDummyCondition(2);

            //a && b
            Decision target = new Decision(cond1, cond2, LogicalOperatorType.And);
            int numberOfAlternativesFound = 0;

            target.GenerateMCDCTestAlternatives(10, true);
            Assert.AreEqual(1, target.MCDCAlternatives.Count);

            PossibleMCDCCombination expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(2, 1, 1, false), new TruthTableRow(2, 2, 0, false), new TruthTableRow(2, 3, 0, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(2, 0, 1, false), new TruthTableRow(2, 2, 0, false), new TruthTableRow(2, 3, 0, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            Assert.AreEqual(1, numberOfAlternativesFound);

            //a || (b && c)
            target = new Decision(cond1, new Decision(cond2, cond3, LogicalOperatorType.And), LogicalOperatorType.Or);
            target.GenerateMCDCTestAlternatives(10, true);
            Assert.AreEqual(1, target.MCDCAlternatives.Count);

            numberOfAlternativesFound = 0;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 0, 1, false), new TruthTableRow(3, 2, 0, false), new TruthTableRow(3, 3, 0, true), new TruthTableRow(3, 4, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 1, 1, false), new TruthTableRow(3, 2, 0, false), new TruthTableRow(3, 3, 0, true), new TruthTableRow(3, 4, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 1, 1, false), new TruthTableRow(3, 2, 0, false), new TruthTableRow(3, 3, 0, true), new TruthTableRow(3, 5, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 1, 1, false), new TruthTableRow(3, 2, 0, false), new TruthTableRow(3, 3, 0, true), new TruthTableRow(3, 6, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;

            Assert.AreEqual(1, numberOfAlternativesFound);
        }

        [TestMethod()]
        public void GenerateAllPossibleMCDCTestAlternativesTestForShortCircuiting()
        {
            Condition cond1 = Condition.GetDummyCondition(0);
            Condition cond2 = Condition.GetDummyCondition(1);
            Condition cond3 = Condition.GetDummyCondition(2);

            //(a && b) || c
            Decision target = new Decision(new Decision(cond1, cond2, LogicalOperatorType.And), cond3, LogicalOperatorType.Or);
            target.GenerateMCDCTestAlternatives(10, true);
            Assert.AreEqual(2, target.MCDCAlternatives.Count);

            PossibleMCDCCombination expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 2, 2, false), new TruthTableRow(3, 4, 0, false), new TruthTableRow(3, 6, 1, true), new TruthTableRow(3, 5, 0, true) });
            Assert.IsTrue(FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives));

            int numberOfAlternativesFound = 0;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 2, 2, false), new TruthTableRow(3, 4, 0, false), new TruthTableRow(3, 6, 1, true), new TruthTableRow(3, 3, 2, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 4, 0, false), new TruthTableRow(3, 6, 1, true), new TruthTableRow(3, 0, 2, false), new TruthTableRow(3, 1, 2, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(3, 4, 0, false), new TruthTableRow(3, 6, 1, true), new TruthTableRow(3, 2, 2, false), new TruthTableRow(3, 1, 2, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;

            Assert.AreEqual(1, numberOfAlternativesFound);
        }

        [TestMethod()]
        public void GenerateAllPossibleMCDCTestAlternativesTestForCoupling()
        {
            Condition cond1 = Condition.GetDummyCondition(0);
            Condition cond2 = Condition.GetDummyCondition(1);
            Condition cond3 = Condition.GetDummyCondition(2);
            Condition cond4 = Condition.GetDummyCondition(3);

            //(a && b) || (!a && c)
            Decision target = new Decision(new Decision(cond1, cond2, LogicalOperatorType.And), new Decision(new Decision(cond3, LogicalOperatorType.Not), cond4, LogicalOperatorType.And), LogicalOperatorType.Or);
            target.ConditionsMap = new Dictionary<string, List<int>>() { { "", new List<int>() { 0, 2 } } };
            target.GenerateMCDCTestAlternatives(10, true);
            Assert.AreEqual(1, target.MCDCAlternatives.Count);

            int numberOfAlternativesFound = 0;
            PossibleMCDCCombination expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 0, 4, false), new TruthTableRow(4, 1, 4, true), new TruthTableRow(4, 11, 1, false), new TruthTableRow(4, 15, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 0, 4, false), new TruthTableRow(4, 1, 4, true), new TruthTableRow(4, 11, 1, false), new TruthTableRow(4, 14, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 0, 4, false), new TruthTableRow(4, 1, 4, true), new TruthTableRow(4, 10, 1, false), new TruthTableRow(4, 14, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 1, 4, true), new TruthTableRow(4, 4, 4, false), new TruthTableRow(4, 11, 1, false), new TruthTableRow(4, 15, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 1, 4, true), new TruthTableRow(4, 4, 4, false), new TruthTableRow(4, 11, 1, false), new TruthTableRow(4, 14, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 1, 4, true), new TruthTableRow(4, 4, 4, false), new TruthTableRow(4, 10, 1, false), new TruthTableRow(4, 14, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 4, 4, false), new TruthTableRow(4, 5, 4, true), new TruthTableRow(4, 11, 1, false), new TruthTableRow(4, 15, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 4, 4, false), new TruthTableRow(4, 5, 4, true), new TruthTableRow(4, 11, 1, false), new TruthTableRow(4, 14, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 4, 4, false), new TruthTableRow(4, 5, 4, true), new TruthTableRow(4, 10, 1, false), new TruthTableRow(4, 14, 3, true) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;

            Assert.AreEqual(1, numberOfAlternativesFound);

            //(a && b) || (a && c)
            target = new Decision(new Decision(cond1, cond2, LogicalOperatorType.And), new Decision(cond3, cond4, LogicalOperatorType.And), LogicalOperatorType.Or);
            target.ConditionsMap = new Dictionary<string, List<int>>() { { "", new List<int>() { 0, 2 } } };
            target.GenerateMCDCTestAlternatives(10, true);
            Assert.AreEqual(1, target.MCDCAlternatives.Count);

            numberOfAlternativesFound = 0;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 10, 0, false), new TruthTableRow(4, 11, 0, true), new TruthTableRow(4, 14, 3, true), new TruthTableRow(4, 4, 5, false) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 10, 0, false), new TruthTableRow(4, 11, 0, true), new TruthTableRow(4, 14, 3, true), new TruthTableRow(4, 1, 5, false) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 10, 0, false), new TruthTableRow(4, 11, 0, true), new TruthTableRow(4, 14, 3, true), new TruthTableRow(4, 5, 5, false) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;
            expected = new PossibleMCDCCombination();
            expected.MCDCRows.AddRange(new List<TruthTableRow>() { new TruthTableRow(4, 10, 0, false), new TruthTableRow(4, 11, 0, true), new TruthTableRow(4, 15, 3, true), new TruthTableRow(4, 5, 5, false) });
            if (FoundPossibleMCDCCombinationInList(expected, target.MCDCAlternatives))
                numberOfAlternativesFound++;

            Assert.AreEqual(1, numberOfAlternativesFound);
        }

        [TestMethod()]
        public void CombinationIsMatchConsideringDontCaresTest()
        {
            // Testing when they are exactly the same (they should be equal)
            PossibleMCDCCombination mcdcCombination1 = new PossibleMCDCCombination();
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 0, 11, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 1, 13, true));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 2, 15, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 3, 17, true));

            PossibleMCDCCombination mcdcCombination2 = new PossibleMCDCCombination();
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 0, 11, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 1, 13, true));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 2, 15, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 3, 17, true));

            Assert.IsTrue(mcdcCombination1.IsMatchConsideringDontCares(mcdcCombination2));

            // Testing when they condition bits are not the same but the have equivalent don't cares (they should be equal)
            mcdcCombination1 = new PossibleMCDCCombination();
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 10, 1, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 3, 2, true));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 12, 4, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, 9, 8, true));

            mcdcCombination2 = new PossibleMCDCCombination();
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 11, 1, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 1, 2, true));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 8, 4, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, 1, 8, true));

            Assert.IsTrue(mcdcCombination1.IsMatchConsideringDontCares(mcdcCombination2));

            // Testing when they are exactly the same but with different don't cares (they shouldn't be equal)
            mcdcCombination1 = new PossibleMCDCCombination();
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, int.MaxValue, 1, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, int.MaxValue - 1, 2, true));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, int.MaxValue - 2, 3, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, int.MaxValue - 3, 4, true));

            mcdcCombination2 = new PossibleMCDCCombination();
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, int.MaxValue, 5, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, int.MaxValue - 1, 6, true));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, int.MaxValue - 2, 7, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, int.MaxValue - 3, 8, true));

            Assert.IsFalse(mcdcCombination1.IsMatchConsideringDontCares(mcdcCombination2));

            // Testing when they are not the same (they shouldn't be equal)
            mcdcCombination1 = new PossibleMCDCCombination();
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 1, 1, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 2, 2, true));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 3, 3, false));
            mcdcCombination1.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 4, 4, true));

            mcdcCombination2 = new PossibleMCDCCombination();
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 5, 1, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 6, 2, true));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 7, 3, false));
            mcdcCombination2.MCDCRows.Add(new TruthTableRow(31, (int.MaxValue / 2) - 8, 4, true));

            Assert.IsFalse(mcdcCombination1.IsMatchConsideringDontCares(mcdcCombination2));
        }

        bool FoundPossibleMCDCCombinationInList(PossibleMCDCCombination comb, List<PossibleMCDCCombination> list)
        {
            foreach (PossibleMCDCCombination element in list)
                if (CompareTwoPossibleMCDCCombinations(comb, element))
                    return true;
            return false;
        }

        bool CompareTwoPossibleMCDCCombinations(PossibleMCDCCombination first, PossibleMCDCCombination second)
        {
            return first.CompareTo(second) == 0;
        }
    }
}
