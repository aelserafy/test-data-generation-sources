﻿/**
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org)
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).
 * 2- The license text must be kept in source files headers. 
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents. 
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/test-data-generation-sources)
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log. 
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).
 * 6- For commercial distribution and use, a license must be obtained from one of the author(s).
 * 
 *  Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.  
 *  In no event will the author(s) be held liable for any damages arising from the use of this software.
*/

using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomGenetic;
using CustomGenetic.Benchmarks;

namespace TestingProject
{
    [TestClass]
    public class RequiredGAGenerationTests
    {
        [TestMethod]
        public void CalculateNumberOfGAGenerationsRequiredTests()
        {
            Environment env = new TriangleClassificationEnv(false);
            env.SetTargetDataType(false);
            int GAGenerationsCountForEqualEqualPredicates = env.GAGenerationsPerEqualityPredicate, GAGenerationsCountForNonEqualEqualPredicates = env.GAGenerationsPerNonEqualityPredicate;
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[0]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[1]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[2]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[3]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[4]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[5]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 6, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[6]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 4, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[7]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 3 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[8]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 4 + GAGenerationsCountForNonEqualEqualPredicates * 6, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[9]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 5 + GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[10]));

            env = new TriangleClassificationEnv(true);
            env.SetTargetDataType(false);
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[0]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[1]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[2]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[3]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[4]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[5]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 8 + GAGenerationsCountForNonEqualEqualPredicates * 15, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[6]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 8 + GAGenerationsCountForNonEqualEqualPredicates * 13, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[7]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 15 + GAGenerationsCountForNonEqualEqualPredicates * 23, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[8]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 16 + GAGenerationsCountForNonEqualEqualPredicates * 24, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[9]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 17 + GAGenerationsCountForNonEqualEqualPredicates * 25, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[10]));

            env = new NextDateEnvWithIf(false);
            env.SetTargetDataType(false);
            Assert.AreEqual(GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[0]));
            Assert.AreEqual(GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[1]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[2]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[3]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 3 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[4]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 4 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[5]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 5 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[6]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 6 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[7]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 7 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[8]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 8 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[9]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 11 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[10]));
            Assert.AreEqual(GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[11]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1 + GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[12]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[13]));

            env = new NextDateEnvWithSwitch();
            env.SetTargetDataType(false);
            Assert.AreEqual(GAGenerationsCountForNonEqualEqualPredicates * 3, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[0]));
            Assert.AreEqual(GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[1]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 7 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[2]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 8 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[3]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 11 + GAGenerationsCountForNonEqualEqualPredicates * 5, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[4]));
            Assert.AreEqual(GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[5]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 1 + GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[6]));
            Assert.AreEqual(GAGenerationsCountForEqualEqualPredicates * 2 + GAGenerationsCountForNonEqualEqualPredicates * 7, env.CalculateNumberOfGAGenerationsRequired(env.AppDecisions[7]));
        }
    }
}
